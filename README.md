# WEB DASHBOARD

Front-end in React for Dashboard

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
nodejs

### Installing
```
npm install
```

## Deployment
```
npm start build
```

Add the folder "build" in S3
