import React from 'react';
import {Switch,Route} from "react-router-dom"
import {Publica} from './app/components/publica_inmueble';
import TerminosyCondiciones from './app/static/terminos_condiciones';
import PoliticaPrivacidad  from './app/static/politica_privacidad';
import Login from './containers/Login/Login';
import LoginVerified from './containers/Login/LoginVerified'
import Home from './containers/Home/Home';
import SignUp from './containers/SignUp/SignUp'
import axios from 'axios'
import configuration from './configuration'
import Loader from 'react-loader'
class App extends React.Component {

  state={
    loading:true,
    token:null,
    userEmail:null
  }


  signupHandler = (emailValue) =>{
    this.setState({
      userEmail:emailValue
    })
  }

  logoutHandler = () =>{
    this.setState({
      userEmail:null
    })
  }



  componentDidMount(){
    let value = localStorage.getItem('token');
    if(value!==null){
        let config={
          headers:{
            'Authorization':value
          }
        }
        axios.get(`${configuration.server}/users/getInfo`,config)
        .then(response=>{
            this.setState({
              userEmail:response.data.email,
              loading:false
            })
        })
        .catch( error=>{
            console.log(error.response.data.message)
            this.setState({
              loading:false
            })
        })
    }else{
      this.setState({
        loading:false
      })
    }
  }

  render() {

    let init = null;
    if(this.state.loading===true){
      init = <Loader loaded={true}/>
    }else{
      init =
      <Switch>

        <Route
            path="/publica_inmueble" exact
            render={(props)=> <Publica {...props} userEmail={this.state.userEmail} signupHandler={this.signupHandler}/>}
        />

        <Route
            path="/login" exact
            render={(props)=> <Login {...props} userEmail={this.state.userEmail} signupHandler={this.signupHandler}/>}
        />


        <Route
            path="/login/:userId" exact
            render={(props)=> <LoginVerified {...props} userEmail={this.state.userEmail} signupHandler={this.signupHandler}/>}
        />




        <Route
            path="/registro" exact
            render={(props)=> <SignUp {...props} userEmail={this.state.userEmail} signupHandler={this.signupHandler}/>}
        />

        <Route
            path={"/"} exact
            render={(props)=> <Home {...props} userEmail={this.state.userEmail} logout={this.logoutHandler}/>}

        />

        <Route
            path={"/politica_privacidad"} exact
            render={(props)=> <PoliticaPrivacidad {...props} userEmail={this.state.userEmail}/>}

        />

        <Route
            path={"/terminos_condiciones"} exact
            render={(props)=> <TerminosyCondiciones {...props} userEmail={this.state.userEmail}/>}

        />

        <Route
            path={"/"} exact
            render={(props)=> <Home {...props} userEmail={this.state.userEmail}/>}

        />

        {/*this does not work*/}
        <Route path='/perfil' component={() => window.location = 'http://behemot.mx/DashboardKlugger/adminpro/gradient-design/mispropiedades.html'}/>

        <Route
              render={
                ()=> (
                    <Home/>
                )
              }
        />

      </Switch>
    }
    return(
      <div>{init}</div>
    );
  }
}

export default App;
