
module.exports = {
  EMAIL_IN_USE : "email already used",
  PASSWORD_CONFIRMATION_FAILED_MESSAGE : "Las contraseñas no coinciden",
  PHONE_BAD_FORMAT_MESSAGE : "Ingresa un teléfono válido",
  EMAIL_IN_USE_MESSAGE : "Usuario ya registrado",
  SELECT_GENDER_MESSAGE : "Selecciona un género",
  LOGIN_ERROR : "user or password incorrect",
  LOGIN_ERROR_MESSAGE: "Usuario o contraseña incorrectos",
  EMAIL_SENT_MESSAGE: "Registro exitoso, se envió un correo de verificación",
  VERIFIED_ERROR: "user does not verified",
  VERIFIED_ERROR_MESSAGE: "Verificar correo"
}
