import React,{Component} from 'react'
import {Redirect} from 'react-router-dom'
import axios from 'axios'
import configuration from '../../configuration'
import Loader from 'react-loader'

class LoginVerified extends Component{
	state = {
		userValid:null,
		loading: true
	}

	componentDidMount(){
		let params = {
			userId: this.props.match.params.userId
		}

		axios.post(`${configuration.server}/users/verified`,params)
		.then ( response=>{
			this.setState({
				userValid:true,
				loading: false
			})
		})
		.catch( error=>{
			console.log(error.response.data);
			this.setState({
				userValid:false,
				loading:false
			})
		})
	}


	render(){
		let init = null;
		if(this.state.loading===true)
			init = <Loader loaded={true}/>
		else if(this.state.userValid===false){
			init = <Redirect to="/login" />
		}else{
			init = <Redirect to={{
				pathname: '/login',
				state: {message:'Usuario Verificado'}
			}}

			/>
		}

		return (
			init
		)
	}
}

export default LoginVerified;