/***
  Component:Login
  Author: Klugger
  Description: Component to start a session with a existing user
********/
import React, { Component } from 'react';
import {Link,Redirect} from 'react-router-dom';
import axios from 'axios'
import configuration from '../../configuration'
import imageBackground from  './images/bg-01.jpg';
import './vendor/bootstrap/css/bootstrap.min.css';
import './fonts/iconic/css/material-design-iconic-font.min.css';
import './fonts/font-awesome-4.7.0/css/font-awesome.min.css';
import './css/util.css';
import './css/main.css';
import {LOGIN_ERROR,LOGIN_ERROR_MESSAGE} from '../../utils/consts'
import {VERIFIED_ERROR,VERIFIED_ERROR_MESSAGE} from '../../utils/consts'
class Login extends Component {

  state = {
      logged:false,
      email:null,
      password:null,
      error: null,
      returnHome:false
  }


  componentDidMount(){
    if(this.props.userEmail!==null && this.props.userEmail!==undefined){
      this.setState({
        logged:true
      })
    }else if(this.props.location!==undefined && this.props.location.state!==undefined){
      this.showMessageError(this.props.location.state.message,3000);
    }
  }

  // handle error

  showMessageError = (message,timeout=2000) =>{
    this.setState({
      error:message
    })
    setTimeout( ()=>{
      this.setState({
          error:null
      })
    },timeout);
  }


  submitLoginHandler = (event) =>{
      event.preventDefault();
      let paramsUser = {
        email:this.state.email.toLowerCase(),
        password: this.state.password
      }

     axios.post(`${configuration.server}/users/signin`,paramsUser)
      .then(response=>{
          localStorage.setItem('token',response.data.token);
          this.props.signupHandler(response.data.user.email);
          this.setState({
            logged:true
          })
      })
      .catch( error=>{
        console.log(error.response.data.message);
        if(error.response.data.message===LOGIN_ERROR){
          this.showMessageError(LOGIN_ERROR_MESSAGE,3000);
        }else if(error.response.data.message===VERIFIED_ERROR){
          this.showMessageError(VERIFIED_ERROR_MESSAGE,3000);
        }
      })
  }

  changeEmailHandler = (event) => {
      this.setState({email:event.target.value})
  }

  changePasswordHandler = (event) => {
      this.setState({password:event.target.value})
  }


  render() {

    return(

      (this.state.logged===true)?
        <Redirect to ="/"/>
      :<div>
      	<link rel="icon" type="image/png" href={require("./images/icons/favicon.ico")}/>


          <div className="limiter">
            <div className="container-login100" style={{backgroundImage:`url(${imageBackground})`}}>
              <div className="wrap-login100Mon p-l-55 p-r-55 p-t-65 p-b-54">
                <form className="login100-form validate-form"  onSubmit={this.submitLoginHandler}>
                      <span className="login100-form-title p-b-49">
                        Bienvenido a Klugger
                      </span>
                      <div className="wrap-input100 validate-input m-b-23">
                        <span className="label-input100">Correo</span>
                        <input onChange={this.changeEmailHandler} className="input100" type="email" required/>
                        <span className="focus-input100" data-symbol="" />
                      </div>
                      <div className="wrap-input100 validate-input">
                        <span className="label-input100">Contraseña</span>
                        <input onChange={this.changePasswordHandler} className="input100" type="password" required/>
                        <span className="focus-input100" data-symbol="" />
                      </div>
                      <div className="text-right p-t-8 p-b-31">
                        <Link to="/">
                          <li>¿Olvidaste tu contraseña</li>
                        </Link>
                      </div>
                      <div className="container-login100-form-btn">
                        <div className="wrap-login100-form-btn">
                          <div className="login100-form-bgbtn" />
                          <button type="submit" className="login100-form-btn">
                              Entrar
                          </button>
                        </div>
                      </div>
                      <p className="error-message-signup">{this.state.error}</p>
                  </form>
                  <div className="txt1 signin-padding text-center p-t-54 p-b-20">
                    <span>
                      Iniciar sesión con
                    </span>
                  </div>
                  <div className="flex-c-m">
                    <Link to="/" className="login100-social-item bg1">
                      <i className="fa fa-facebook" />
                    </Link>
                    <Link to="/" className="login100-social-item bg2">
                      <i className="fa fa-twitter" />
                    </Link>
                    <Link to="/" className="login100-social-item bg3">
                      <i className="fa fa-google" />
                    </Link>
                  </div>
                    <div className="flex-col-c p-t-155">
                      <span className="txt1 p-b-17">
                        Si aun no tienes cuenta
                      </span>
                      <Link to="/registro" className="txt2">
                        REGÍSTRATE AQUÍ
                      </Link>
                      <Link to="/" className="txt2">
                        REGRESAR
                      </Link>
                    </div>
              </div>
            </div>
            </div>
            <div id="dropDownSelect1" />
      </div>
    );
  }
}

export default Login;
