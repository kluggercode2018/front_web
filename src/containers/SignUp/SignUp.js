/***
  Component: SignUp
  Author: Klugger
  Description: Component to add a new user in the platform
    Fields:
      Name,lastName,phoneNumer,email,password,gender
    The register is going to need verification
********/
import React from 'react';
import {Redirect} from  'react-router-dom';
import imageBackground from  './images/bg-01.jpg';
import axios from 'axios'
import Radium from 'radium'
import configuration from '../../configuration'
import '../Login/vendor/bootstrap/css/bootstrap.min.css';
import '../Login/fonts/iconic/css/material-design-iconic-font.min.css';
import '../Login/fonts/font-awesome-4.7.0/css/font-awesome.min.css';
import '../Login/css/util.css';
import '../Login/css/main2.css';

import './SignUp.css'

import {EMAIL_IN_USE,EMAIL_IN_USE_MESSAGE,EMAIL_SENT_MESSAGE} from '../../utils/consts'
import {PASSWORD_CONFIRMATION_FAILED_MESSAGE} from '../../utils/consts'
import {SELECT_GENDER_MESSAGE} from '../../utils/consts'


class SignUp extends React.Component {

  state = {
      logged:false, // to redirect if the user is logged

      firstName: "",
      lastName: "",
      phoneNumer :"",
      password: "",
      passwordConfirmation: "",
      gender: "",
      maleGenderSelected: "",
      femaleGenderSelected: "",
      error: null, // validations
      returnHome: false

  }

  componentDidMount(){
    if(this.props.userEmail!==null && this.props.userEmail!==undefined){
      this.setState({
        logged:true
      })
    }
  }


  firstNameChangeHandler =(event) =>{
    this.setState({
      firstName:event.target.value
    })
  }

  lastNameChangeHandler =(event) =>{
    this.setState({
      lastName:event.target.value
    })
  }



  phoneNumerChangeHandler =(event) =>{
    this.setState({
      phoneNumer:event.target.value
    })
  }


  emailChangeHandler =(event) =>{
    this.setState({
      email:event.target.value
    })
  }


  passwordChangeHandler =(event) =>{
    this.setState({
      password:event.target.value
    })
  }

  passwordConfirmationChangeHandler=(event)=>{
    this.setState({
      passwordConfirmation:event.target.value
    })
  }

  selectionGenderHandler = (gender) =>{
    if(gender===0){
      this.setState({
        femaleGenderSelected:"gender-selected",
        maleGenderSelected:"",
        gender: "0"
      })
    }else{
      this.setState({
        maleGenderSelected:"gender-selected",
        femaleGenderSelected:"",
        gender: "1"
      })
    }
    console.log(this.state.gender);
  }


  showMessageError = (message,timeout=3500)=>{
    this.setState({
      error:message
    })
    setTimeout( ()=>{
      this.setState({
        error:null
      })
    },timeout)
  }
  /// sumbit
  newUserHandler=(event)=>{
    event.preventDefault();
    if(this.state.password!==this.state.passwordConfirmation){
      this.showMessageError(PASSWORD_CONFIRMATION_FAILED_MESSAGE)
      return;
    }
    if(this.state.gender!=="0" && this.state.gender!=='1'){
      this.showMessageError(SELECT_GENDER_MESSAGE)
      return;
    }
    let genderBoolean = (this.state.gender==="0")?false:true;

    let paramsNewUser = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      password: this.state.password,
      email: this.state.email.toLowerCase(),
      mobile: parseInt(this.state.phoneNumer),
      gender: genderBoolean
    }


    axios.post(`${configuration.server}/users/signup`,paramsNewUser)
    .then(response=>{
        this.showMessageError(EMAIL_SENT_MESSAGE,5000)
        /**

        localStorage.setItem('token',response.data.token);
        this.props.signupHandler(response.data.user.email);
        this.setState({
          logged:true
        })

        */
    })
    .catch( error=>{
      console.log(error.response.data.message);
      if(error.response.data.message===EMAIL_IN_USE){
        this.showMessageError(EMAIL_IN_USE_MESSAGE);
      }
    })
  }



  returnButtonHandler=() =>{
    this.setState({
      returnHome: true
    })
  }



  render() {

    let styleReturnButton ={
      fontSize: 25,
      fontWeight: 'bold',
      float: 'right',
      ':hover': {
        cursor:'pointer'
      }
    }


    return(
            (this.state.logged===true)?
            <Redirect to="/"/>:
            (this.state.returnHome===true)?
            <Redirect to="/"/>:
            <div className="limiter">
              <div  className="container-login100" style={{backgroundImage: `url(${imageBackground})`}}>
                <div className="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">

                  <form onSubmit={this.newUserHandler}  className="login100-form validate-form">
                    <span className="login100-form-title p-b-49">
                      Únete a esta gran comunidad
                    </span>
                    <br />
                    <div className="wrap-input100 validate-input m-b-23">
                      <span className="label-input100">Nombre (s)</span>
                      <input minLength="3" onChange={this.firstNameChangeHandler} className="input100" type="text" required/>
                      <span className="focus-input100" data-symbol="" />
                    </div>
                    <div className="wrap-input100 validate-input">
                      <span className="label-input100">Apellidos</span>
                      <input minLength="3" onChange={this.lastNameChangeHandler} className="input100" type="text" required/>
                      <span className="focus-input100" data-symbol="" />
                    </div>
                    <br />

                    <div className="signup-button-center">
                        <div className="container-login100-form-btn-signup">
                          <div className="wrap-login100-form-btn1">
                            <div className="login100-form-bgbtn" />
                            <button type="button" className={"login100-form-btn "  + this.state.femaleGenderSelected}  onClick={()=>this.selectionGenderHandler(0)}>
                              Mujer
                            </button>
                          </div>
                        </div>
                        <div className="container-login100-form-btn-signup">
                          <div className="wrap-login100-form-btn1">
                            <div className="login100-form-bgbtn" />
                            <button type="button"  className={"login100-form-btn " + this.state.maleGenderSelected} onClick={()=>this.selectionGenderHandler(1)}>
                              Hombre
                            </button>
                          </div>
                        </div>
                    </div>
                    <div className="wrap-input100 validate-input">
                      <span className="label-input100">Teléfono</span>
                      <input minLength="10" onChange={this.phoneNumerChangeHandler} className="input100" type="number" required/>
                      <span className="focus-input100" data-symbol="✆" />
                    </div>
                    <div className="wrap-input100 validate-input">
                      <span className="label-input100">Correo electrónico</span>
                      <input className="input100" onChange={this.emailChangeHandler} type="email" required/>
                      <span className="focus-input100" data-symbol="@" />
                    </div>
                    <div className="wrap-input100 validate-input">
                      <span className="label-input100">Contraseña</span>
                      <input minLength="6" onChange={this.passwordChangeHandler} className="input100" type="password" required/>
                      <span className="focus-input100" data-symbol="" />
                    </div>
                    <div className="wrap-input100 validate-input">
                      <span className="label-input100">Confirmar contraseña</span>
                      <input minLength="6" onChange={this.passwordConfirmationChangeHandler} className="input100" type="password" required/>
                      <span className="focus-input100" data-symbol="" />
                    </div>
                    <br />
                    <div className="container-login100-form-btn">
                      <div className="wrap-login100-form-btn">
                        <div className="login100-form-bgbtn" />
                        <button type="submit" className="login100-form-btn">
                          Registrarme
                        </button>
                      </div>
                    </div>
                    <br/>
                    <p style={styleReturnButton} onClick={this.returnButtonHandler}>Regresar</p>
                      <p className="error-message-signup">{this.state.error}</p>
                  </form></div>
              </div>
            </div>


    );
  }
}

export default Radium(SignUp);
