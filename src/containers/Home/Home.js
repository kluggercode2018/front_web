/***
  Component: Home
  Author: Klugger
  Description: Component to the information about Klugger (Services, About us,...)
********/
import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import { HashLink  } from 'react-router-hash-link';
import QuickSale from '../../components/QuickSale/QuickSale'
import ModalInfo from '../../components/Modals/ModalInfo/ModalInfo'


import './css/font-awesome/css/font-awesome.min.css';
import './css/bootstrap/bootstrap.min.css'
import './css/linearicons.css';
import './css/iconmoon.css';
import './css/animat/animate.min.css'
import './css/fancybox/jquery.fancybox.css';
import './css/nivo-lightbox/nivo-lightbox.css';
import './css/themes/default/default.css';
import './css/owl-carousel/owl.carousel.css';
import './css/owl-carousel/owl.theme.css';
import './css/owl-carousel/owl.transitions.css';
import './css/style.css';
import './css/responsive.css';


class Home extends Component {
  state={
    showModalInfo:true
  }

  modalClosedHandler = () =>{
    this.setState({
      showModalInfo:false
    })
  }

  logoutHandler = (event) =>{
    event.preventDefault();
    localStorage.removeItem('token')
    this.props.logout();

  }


  render() {

    let lastLinks = null;
    if(this.props.userEmail===null  || this.props.userEmail===undefined){
      lastLinks =
        <Link to="/login"
          className="link-nav"
          >
          <li  >Login </li>
        </Link>

    }else{
      //
      // let lastLink =
      //   <Link to="/publica_inmueble"
      //     className="link-nav"
      //     >
      //     <li> Publicar inmueble </li>
      //   </Link>
       let logout =
       <Link to="/" onClick={this.logoutHandler} className="link-nav">
          <li> Cerrar Sesión </li>
        </Link>
       let dashboard =
       <a href="http://behemot.mx/DashboardKlugger/adminpro/gradient-design/mispropiedades.html" className="link-nav">
          <li> Mi perfil </li>
       </a>
        lastLinks = [dashboard,logout]
    }
    let modalInfo = null;
    if(this.state.showModalInfo===true && this.props.userEmail===null){
      modalInfo = <ModalInfo
          clicked={this.modalClosedHandler}
          />
    }
    return (
      <div>
          {/*<div className="preloader"><div className="loaded">&nbsp;</div></div>*/}
          <header id="home" className="header">
            <div className="main_menu_bg navbar-fixed-top">
              <div className="container">
                <div className="row">
                  <div className="nave_menu">
                    <nav className=" class-padding-navbar navbar navbar-default">
                      <div className="container-fluid">
                        {/* Brand and toggle get grouped for better mobile display */}
                        <div className="navbar-header">
                          <div className="Opacity" />
                          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span className="sr-only">Klugger</span>
                            <span className="icon-bar" />
                            <span className="icon-bar" />
                            <span className="icon-bar" />
                          </button>


                        </div>
                        {/* Collect the nav links, forms, and other content for toggling */}
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


                          <div className="logo class-display-logo">
                            <img src={require("./images/logo.png")} alt="" />
                          </div>

                          <ul className="nav navbar-nav navbar-right montse1">
                          <HashLink
                            className="link-nav"
                            to="/#home"
                            scroll={el => el.scrollIntoView({ behavior: 'smooth', block: 'end' })}
                          >
                              <li  >Inicio </li>
                          </HashLink>

                            <HashLink
                              className="link-nav"
                              to="/#features"
                              scroll={el => el.scrollIntoView({ behavior: 'smooth', block: 'end' })}
                            >
                                <li  >Nosotros </li>
                            </HashLink>

                            <HashLink
                              className="link-nav"
                              to="/#works"
                              scroll={el => el.scrollIntoView({ behavior: 'smooth', block: 'end' })}
                            >
                                <li  >Servicios </li>
                            </HashLink>



                            <HashLink
                              className="link-nav"
                              to="/#features"
                              scroll={el => el.scrollIntoView({ behavior: 'smooth', block: 'end' })}
                            >
                                <li  >Contacto </li>
                            </HashLink>

                            {lastLinks}

                          </ul>
                        </div>{/* /.navbar-collapse */}
                      </div>{/* /.container-fluid */}
                    </nav>
                  </div>
                </div>{/*End of row */}
              </div>{/*End of container */}
            </div>
          </header> {/*End of header */}








      <section id="banner" className="banner">
        <div className="container">
          <div className="row">
            <div className="main_banner_area">
              <div className="col-md-6 col-sm-6">
                <div className="single_banner wow fadeIn">
                  <img src={require("./images/iphone1.png")} alt="" />
                </div>
              </div>
              <div className="col-md-6 col-sm-6">
                <div className="single_banner_text wow zoomIn" data-wow-duration="1s">
                  <h2 style={{textAlign:'center'}}>Próximamente descarga nuestra App</h2>
                  {/*<p>Ya no pierdas tiempo buscando en tantos sitios, nosotros detectamos el lugar perfecto para que tu vida sea fácil. </p>*/}
                  {/*
                  <div className="apps_downlod">
                    <a href><img src={require("./images/ap.png")} alt="" /></a>
                    <a href><img src={require("./images/ag.png")} alt="" /></a>
                  </div>
                  */}
                </div>
              </div>
              <div className="scrolldown">
                <a href="#features" className="scroll_btn"></a>
              </div>
            </div>
          </div>
        </div>
      </section>{/* End of Banner Section */}








      {/* Video Section */}
      <section id="video" className="video_area wow fadeIn" data-wow-duration="2s" data-wow-dealy="1.5s">
        <div className="container">
          <div className="row">
            <div className="main_video_content text-center">
              <div className="head_title">
                <h2>¿Cómo funciona?</h2>
                <p>Encuentra el hogar de tus sueños más rápido. Nosotros te ayudamos buscando, entendiendo y aprovechar todo los factores que influyen en tu localidad.</p>
              </div>
              <div className="col-md-12 col-sm-12">
                <div className="icon_area">
                  <a href="" className="youtube-media"><img src={require("./images/play.png")} alt="play-icon" /></a>
                </div>
              </div>
              <p>Descarga nuestra App ¡Gratis!<a href="#home"> </a></p>
            </div>
          </div>
        </div>
      </section>






        {/*Abouts Section */}
        <section id="abouts" className="abouts">
          <div className="container">
            <div className="row">
              <div className="abouts_content">
                <div className="col-md-6 col-sm-6 col-xs-12">
                  <div className="single_abouts_img text-center wow fadeInLeft" data-wow-duration="1s">
                    <img src={require("./images/abouts.gif")} alt="" />
                  </div>
                </div>
                <div className="col-md-6 col-sm-6 col-xs-12">
                  <div className="single_abouts_text wow fadeInRight" data-wow-duration="1s">
                    <h2>Encuentra el hogar de tus sueños más rápido.</h2>
                    <p>No pierdas tiempo en multiples páginas inmobiliarias, tenemos toda la información que necesitas para que eligas tu casa más rápido.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>{/* End of Abouts Section */}




        <section id="features" className="features">
        <div className="container">
          <div className="row">
            <div className="main_features">
              <div className="col-md-6 col-sm-6 col-xs-12">
                <div className="single_features_text wow fadeIn" data-wow-duration="2s">
                  <h2>Ya no busques casa, deja que te encuentre.</h2>
                  <br />
                  <p>Encontramos las mejores propiedades para ti, tomando en cuenta tus gustos, tu estilo de vida y tus preferencias.</p>
                </div>
              </div>
              <div className="col-md-6 col-sm-6 col-xs-12">
                <div className="single_features_img wow fadeIn" data-wow-duration="2s">
                  <img src={require("./images/features.gif")} alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>{/* End of Features Section */}








        <section id="works" className="works">
            <div className="container">
              <div className="row">
                <div className="main_works">

                  <div className="col-md-4 col-sm-12 col-xs-12">
                    <div className="single_works text-right wow fadeInLeft" data-wow-duration="2s">
                      <div className="single_works_deatels">
                        <img src={require("./images/logo/serv.png")}  alt="" />
                        <h5>SERVICIOS</h5>
                        <p>Vive cerca de todos los servicios de tu preferencia. ¡Disfruta más lo que te rodea!</p>
                      </div>
                      <div className="single_works_deatels">
                        <img src={require("./images/logo/transito.png")}  alt="" />
                        <h5>TRÁNSITO</h5>
                        <p>Ya no pierdas tanto tiempo de traslado a tus sitios recurrentes. ¡Vive cerca!</p>
                      </div>
                      <div className="single_works_deatels">
                        <img src={require("./images/logo/police.png")}  alt="" />
                        <h5>SEGURIDAD</h5>
                        <p>Conoce el nivel de seguridad de tu colonia y los puntos ayuda ciudadana.</p>
                      </div>
                    </div>
                  </div>

                  <div className="col-md-4 col-sm-12 col-xs-12">
                    <div className="pendientsizegif single_works_text_middel text-center wow fadeInDown" data-wow-duration="2s">
                      <img src={require("./images/workiphone.gif")} alt="" />
                    </div>
                  </div>


                  <div className="col-md-4 col-sm-12 col-xs-12">
                    <div className="single_works text-left wow fadeInRight" data-wow-duration="2s">
                      <div className="single_works_deatels">
                        <img src={require("./images/logo/buy.png")}  alt="" />
                        <h5>PRECIO</h5>
                        <p>Detecta el precio del mercado de tu propiedad. ¡Vende o renta más rápido!.</p>
                      </div>
                      <div className="single_works_deatels">
                        <img src={require("./images/logo/m2.png")}  alt=""/>
                        <h5>ESPACIO</h5>
                        <p>Encontramos lo que necesitas con el espacio perfecto para ti y tu familia.</p>
                      </div>
                      <div className="single_works_deatels">
                        <img src={require("./images/logo/carro.png")} alt=""/>
                        <h5>NECESIDADES</h5>
                        <p>Te ayudamos a encontrar la casa con las amenidades de tu preerencia.</p>
                      </div>{/* End of single works deatels */}
                    </div>
                  </div>{/* End of col-md-4 */}


                </div>{/* End of main Works */}
              </div>{/* End of row */}
            </div>{/* End of container */}
          </section>


          {(this.props.userEmail===null || this.props.userEmail===undefined)?<QuickSale/>:null}




            {/* 	Que el tiempo no sea para buscar, sino para aprovechar. */}
          <section id="joinus" className="joinus wow zoomIn">
            <div className="container">
              <div className="row">
                <div className="main_joine_content text-center montse2">
                  <div className="head_title">
                    <h2>Personaliza con tus preferencias</h2>
                    {/*<p>Descarga nuestra aplicación y encuentra más rápido. Que el tiempo no sea para buscar, sino para disfrutar.</p>*/}
                    <p> Descarga nuestra aplicación próximamente </p>
                  </div>
                  {/*
                  <div className="main_joinus">
                    <a href><i className="fa fa-apple" />App Store</a>
                    <a href><i className="fa fa-android" />Play Store</a>
                  </div>
                  */}
                </div>
              </div>
            </div>
          </section>





          {/* footer Section */}
          <footer  className="footerMon">
            <div className="container">
              <div className="row wow zoomIn" data-wow-duration="2s">
                <div className="col-md-6 col-sm-6 col-xs-12">
                  <div className="single_footer">
                    <p className="footerPMon">Recibe noticias sobre el sector inmobiliario, urbano y tecnológico.</p>
                  </div>
                </div>
                <div className="col-md-6 col-sm-6 col-xs-12">
                  <div className="single_footer text-right">
                    <div className="footer_menu">
                      <ul>
                        <Link className="link-footer" to="/terminos_condiciones"> Términos y condiciones </Link>
                        <Link className="link-footer" to="/politica_privacidad"> Aviso de privacidad </Link>
                      </ul>
                    </div>
                    <div className="footer_socail">
                      <a href="https://www.facebook.com/Klugger_oficial-750444491968685/?modal=admin_todo_tour" target="__blank"><i  className="iconfooterklugger fa fa-facebook" /></a>
                      <a href="https://twitter.com/klugger_app"><i className="iconfooterklugger fa fa-twitter" target="__blank"/></a>
                      <a href="https://www.linkedin.com/in/klugger-oficial-0b38aa172/&quot;" target="__blank"><i className="iconfooterklugger fa fa-linkedin" /></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </footer>




          {/* STRAT SCROLL TO TOP */}
          <div className="scrollup">
            <a href="#home"><i className="fa fa-chevron-up" /></a>
          </div>
          {modalInfo}
      </div>
    );
  }
}

export default Home;
