import React, { Component } from 'react';
import Backdrop from '../Backdrop/Backdrop';
import '../Modal.css'
import './ModalSignUp.css'
import axios from 'axios'
import configuration from '../../../configuration'

class ModalInfo extends Component {

    state = {
      email:null,
      message:null
    }


    showMessageHandler = (message)=>{
      this.setState({
        message
      })
      setTimeout( ()=>{
        this.setState({
          message:null
        })
        this.props.clicked();
      },2000)
    }

    submitHandler= event =>{
      event.preventDefault();

      let paramsSubscribe = {
        email: this.state.email.toLowerCase()
      }


      axios.post(`${configuration.server}/admin/subscribe`,paramsSubscribe)
      .then(response=>{
          this.showMessageHandler('Felicidades! Ya estás suscrito')
      })
      .catch( error=>{
        console.log(error);
        console.log(error.response.data.message);
        this.props.clicked();
      })
    }

    changeEmailHandler = event =>{
      this.setState({
        email:event.target.value
      })
    }


    closeButtonHandler = () =>{
        this.props.clicked();
    }
    render(){
        return (
            <div className="ModalSignUp">
              <Backdrop clicked={this.props.clicked} />
              <div className="Modal">
                  <div className="close-button" onClick={this.closeButtonHandler}>X</div>
                  <form onSubmit={this.submitHandler}>
                    <img src={require("../../../containers/Home/images/logo.png")} alt="" />
                    <p className="p-signupHandler" >Suscríbete y recibe novedades sobre el sector inmobiliario.</p>
                    <input className="input-modal-signup" placeholder="Correo electrónico" type="email" onChange={this.changeEmailHandler} required/>
                    <input className="button-modal-signup" type="submit" value="ENVIAR"/>
                  </form>
                  <p className="message-subscribe">
                    {this.state.message}
                  </p>
              </div>
          </div>
        )
    }
}

export default ModalInfo;
