import React from 'react';
import {Link} from 'react-router-dom'

const QuickSale = (props) =>{

  return (
    <section id="customiz" className="customiz wow fadeIn" data-wow-duration="2s">
      <div className="container">
        <div className="row">
          <div className="main_customiz">
            <div className="col-md-6 col-md-offset-6 col-sm-9 col-sm-offset-3 col-xs-8 col-xs-offset-4">
              <div className="single_customiz">
                <h2>¿Quiéres que tu casa se venda más rápido?</h2>
                <br />
                <Link to="/registro"><li className="buttonSignUp"> Regístrate</li></Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default QuickSale;
