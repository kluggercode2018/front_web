var axios = require('axios');

var property_url = 'https://tnha22duoj.execute-api.us-east-1.amazonaws.com/prod';
module.exports = {

    publishProperty: function(data) {
        var url = property_url + '/properties/upload/property';
        var encodedURI = window.encodeURI(url);
        var config = {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        };
        return axios({
          method:'POST',
          url: encodedURI,
          data:{
            suburb: data.suburb,
            propertyType: data.propertyType,
            operation: data.operation,
            propertyState: data.propertyState,
            municipality: data.municipality,
            street: data.street,
            pc: data.pc,
            externalNumber: data.externalNumber,
            internalNumber: data.internalNumber,
            externalNumberVisibility: data.externalNumberVisibility,
            internalNumberVisibility: data.internalNumberVisibility,
            landSize: data.landSize,
            landSizeUnit: data.landSizeUnit,
            bedrooms: data.bedrooms,
            bathrooms: data.bathrooms,
            halfBathrooms: data.halfBathrooms,
            parkingLots: data.parkingLots,
            antiqueness: data.antiqueness,
            floors: data.floors,
            gardenSize: data.gardenSize,
            gardenSizeUnit: data.gardenSizeUnit,
            constructionSize: data.constructionSize,
            constructionSizeUnit: data.constructionSizeUnit,
            air: data.air,
            elevator: data.elevator,
            gym: data.gym,
            heat: data.heat,
            laundry: data.laundry,
            swimmingPool: data.swimmingPool,
            security: data.security,
            roofgarden: data.roofgarden,
            price : data.price,
            priceType: data.priceType,
            phone: data.phone,
            mail: data.mail,
            description: data.description,
            lat: data.lat,
            lng: data.lng,
            priceUnit: data.priceUnit,
            imagesUrls:data.imagesUrls,
            propertyId:data.propertyId,
            pageSource:data.pageSource
          },
          config,

        })
        .then(function (response) {
            return response.data;

        })
        .catch(function (error) {
            console.log(error);
        });
    },

    municipalities: function(data){
      var url = property_url + '/addresses/municipalities';
      var encodedURI = window.encodeURI(url);
      var config = {
        headers: {
          'X-My-Custom-Header': 'Header-Value'
        }
      };
      return axios({
        method:'POST',
        url: encodedURI,
        data:{
          estadoId: data,
        },
        config
      })
      .then(function (response) {
          return response.data;
      })
      .catch(function (error) {
          console.log(error);
      });
    },

    neighbothoodCp: function(data,data2){
      var url = property_url + '/addresses/neighborhood';
      var encodedURI = window.encodeURI(url);
      var config = {
        headers: {
          'X-My-Custom-Header': 'Header-Value'
        }
      };
      return axios({
        method:'POST',
        url: encodedURI,
        data:{
          estadoId: data,
          municipioId:data2
        },
        config
      })
      .then(function (response) {
          return response.data;
      })
      .catch(function (error) {
          console.log(error);
      });
    },

    deleteImage: function(data, data2){
      var url = property_url + '/properties/erase/image';
      var encodedURI = window.encodeURI(url);
      var config = {
        headers: {
          'X-My-Custom-Header': 'Header-Value'
        }
      };
      return axios({
        method:'POST',
        url: encodedURI,
        data:{
          nameFile: data,
          propertyId:data2
        },
        config
      })
      .then(function (response) {
          return response.data;
      })
      .catch(function (error) {
          console.log(error);
      });
    },

    radius: function(data){
      var url = property_url + '/properties/query/radius';
      var encodedURI = window.encodeURI(url);
      var config = {
        headers: {
          'X-My-Custom-Header': 'Header-Value'
        }
      };
      return axios({
        method:'POST',
        url: encodedURI,
        data:{
          propertyState: "durango",
          municipality:"2",
          lat:19.3242424,
          lng:-99.4324324,
          km:2000
        },
        config
      })
      .then(function (response) {
          return response.data;
      })
      .catch(function (error) {
          console.log(error);
      });
    },

    rect: function(obj){
      var url = property_url + '/properties/query/rect';
      var encodedURI = window.encodeURI(url);
      var config = {
        headers: {
          'X-My-Custom-Header': 'Header-Value'
        }
      };

      return axios({
        method:'POST',
        url: encodedURI,
        data:{
          latMin: obj.latMin,
          latMax: obj.latMax,
          lngMin: obj.lngMin,
          lngMax: obj.lngMax,
          propertyType: obj.propertyType,
          operationType: obj.operationType,
          constructionSizeMin: obj.constructionSizeMin,
          constructionSizeMax: obj.constructionSizeMax,
          bedroomsMin: obj.bedroomsMin,
          bedroomsMax: obj.bedroomsMax,
          bathroomsMin: obj.bathroomsMin,
          bathroomsMax: obj.bathroomsMax,
          parkingLotsMin: obj.parkingLotsMin,
          parkingLotsMax: obj.parkingLotsMax,
          priceMin: obj.priceMin,
          priceMax: obj.priceMax
        },
        config
      })
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
          console.log(error);
      });
    },

    info: function(data, data1){
      var url = property_url + '/properties/info';
      var encodedURI = window.encodeURI(url);
      var config = {
        headers: {
          'X-My-Custom-Header': 'Header-Value'
        }
      };
      return axios({
        method:'POST',
        url: encodedURI,
        data:{
          propertyId: data,
          hashKey: data1
        },
        config
      })
      .then(function (response) {
          return response.data;
      })
      .catch(function (error) {
          console.log(error);
      });
    },

    contact: function(data){
      var url = property_url + '/properties/notify/owner';
      var encodedURI = window.encodeURI(url);
      var config = {
        headers: {
          'X-My-Custom-Header': 'Header-Value'
        }
      };
      return axios({
        method:'POST',
        url: encodedURI,
        data:{
          name:data.name,
          email:data.email,
          ownerEmail:data.ownerEmail,
          phone:data.phone,
          url:data.url
        },
        config
      })
      .then(function (response) {
          return response.data;
      })
      .catch(function (error) {
          console.log(error);
      });
    },

    facebook: function(data){
      var url = property_url + '/users/auth/facebook';
      var encodedURI = window.encodeURI(url);
      
      return axios.get(encodedURI)
      .then(function (response) {
          return response.data;
      })
      .catch(function (error) {
          console.log(error);
      });
    }

}
