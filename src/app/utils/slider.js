import $ from 'jquery';

function slider(){
  return(
    $('.slider-for').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: false,
     fade: true,
     asNavFor: '.slider-nav',
    autoplay: true
  }),

   $('.slider-nav').slick({
     slidesToShow: 4,
     slidesToScroll: 1,
     asNavFor: '.slider-for',
     dots: false,
     centerMode: true,
     focusOnSelect: true,
    infinite: true,
    centerPadding: '0px'
   })
  )

}
