import React from 'react';
import ReactDOM from 'react-dom';
import valuacionesCertificadas from '../../media/valuaciones_certificadas.png';
import avaluosBasicos from '../../media/avaluos_basicos.png';
import avaluosPremium from '../../media/avaluos_premium.png';
import estudioMercado from '../../media/estudio_mercado.png';


export class Services extends React.Component {
  componentDidMount(){
    ReactDOM.findDOMNode(document.body).scrollIntoView();
  }
  render() {
    return (
      <section className="background-static">
        <div className="services-content">
          <div className="card">
            <div className="imgBx">
              <img src={avaluosBasicos} alt=""/>
              <h1>Avaluos basicos</h1>
            </div>
            <div className="details">
              <h2>What is Lorem Ipsum?</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
          </div>
          <div className="card">
            <div className="imgBx">
              <img src={avaluosPremium} alt=""/>
              <h1>Avaluos premium</h1>
            </div>
            <div className="details">
              <h2>What is Lorem Ipsum?</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
          </div>
          <div className="card">
            <div className="imgBx">
              <img src={valuacionesCertificadas} alt=""/>
              <h1>Valuaciones certificadas</h1>
            </div>
            <div className="details">
              <h2>What is Lorem Ipsum?</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
          </div>
          <div className="card">
            <div className="imgBx">
              <img src={estudioMercado} alt=""/>
              <h1>Estudios de mercado</h1>
            </div>
            <div className="details">
              <h2>What is Lorem Ipsum?</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Services;
