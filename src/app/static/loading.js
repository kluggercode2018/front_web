import React from 'react';
import loading from '../../media/loading.gif';


export class Loading extends React.Component {

  render() {
    return (
      <div className="loading">
        <div className="loading-background">
          <div className="loading-content">
            <img src={loading} alt="gif" />
          </div>
        </div>
      </div>
    );
  }
}

export default Loading;
