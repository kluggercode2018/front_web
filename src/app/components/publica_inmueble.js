import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import api from'../utils/api';
import update from 'immutability-helper';
import Dropzone from 'react-dropzone';
import request from 'superagent';
import _ from 'lodash';
import Footer from '../../layout/footer';
import Header from '../../layout/header';
import '../css/main.css'
import {Redirect} from 'react-router-dom'
import InputRange from 'react-input-range';

//crear propertyId
var uuidv5 = require('uuid/v5');

const CLOUDINARY_UPLOAD_URL = 'https://tnha22duoj.execute-api.us-east-1.amazonaws.com/prod/properties/upload/image';

export class Publica extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      hasErrors: false,
      logged:true,
      hasErrorImages:false,
      uploadedFile: null,
      dataMuni:{},
      uploadedFileCloudinaryUrl: [],
      NameURL:[],
      isChecked: false,
      validated: false ,
      values:{
        stateRes:"",
        municipalityRes:""
      },
      publish_data: {
        suburb: "",
        propertyType: "",
        operation: "",
        propertyState: "",
        municipality: "",
        street: "",
        pc: "",
        externalNumber: "",
        internalNumber: "",
        externalNumberVisibility: true,
        internalNumberVisibility: false,
        landSize: '',
        landSizeUnit: "m2",
        bedrooms: 5,
        bathrooms: 5,
        halfBathrooms: 5,
        parkingLots: 5,
        antiqueness: 50,
        floors: 5,
        gardenSize: 50,
        gardenSizeUnit: "m2",
        constructionSize: '',
        constructionSizeUnit: "m2",
        air: false,
        elevator: false,
        gym: false,
        heat: false,
        laundry: false,
        swimmingPool: false,
        security: false,
        roofgarden: false,
        price : '',
        phone: '',
        mail: '',
        description: '',
        lat: '',
        lng: '',
        priceUnit:'MXN',
        checked: false,
        priceType:true,
        propertyId:'',
        imagesUrls:[],
        pageSource:"Klugger"
      }
    };
    this.reset = this.reset.bind(this);
  }

  componentDidMount(){
    if(this.props.userEmail===null || this.props.userEmail===undefined){
      this.setState({
        logged:false
      })
    }
    ReactDOM.findDOMNode(document.body).scrollIntoView();
  }

  onImageDrop(files) {
    this.setState({
      uploadedFile: files
    });

    this.handleImageUpload(files);
  }

  handleImageUpload(file) {
    var propertyIdImage = this.state.propertyId;
    let upload = request.post(CLOUDINARY_UPLOAD_URL)
                    .field('propertyId',propertyIdImage)
                    //.field('propertyId','5345353')
                    .field('image', file);

    upload.end((err, response) => {
      if( !err ){
        var reponseUrl = response ? response.body.url :[];
        var arr = [];

        if( this.state.uploadedFileCloudinaryUrl.length === 0 ){
            arr.push( reponseUrl );
        }else{
            arr.push( reponseUrl );
            arr = arr.concat( this.state.uploadedFileCloudinaryUrl );
        }
        this.setState({
          uploadedFileCloudinaryUrl : arr
        });

        var nameUrl = this.state.uploadedFile ? this.state.uploadedFile[0].name : [];
        var arrName = [];

        if( this.state.NameURL.length === 0 ){
            arrName.push( nameUrl );
        }else{
            arrName.push( nameUrl );
            arrName = arrName.concat( this.state.NameURL );
        }
        this.setState({
          NameURL : arrName
        });
      }else{
          console.log("warning")
      }

    });
  }

  closeModal(){
    $(".static--form-images").fadeOut();
    $(".modal-error-lat").fadeOut();
  }

  geocode = (google) => {
    var geocoder = new google.maps.Geocoder();
    var str = this.state.publish_data.street;
    var suburb = this.state.publish_data.suburb;
    var num_ext = this.state.publish_data.externalNumber;
    var pc = this.state.publish_data.pc;
    var state = this.state.publish_data.propertyState;
    var address = str + ' ' + num_ext + ', '+ suburb + ', ' + pc + ' ' + state;

    geocoder.geocode( { 'address': address}, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          //console.log("Coordenadas: " + results[0].geometry.location.lat() + ", " + results[0].geometry.location.lng());
          this.setState({
            publish_data: update(this.state.publish_data, {
              lat: {$set: results[0].geometry.location.lat()},
              lng: {$set: results[0].geometry.location.lng()}
            })
          });
        }
        else {
        //console.log("Geocode was not successful for the following reason: " + status);
        }
    });
  }

  spaceReplaceMap(string){
    return string.replace(' ', '%20')
  }

  newPublishProperty(files){

    var info = this.state;
    var construc = info.publish_data.constructionSize
    var terreno = info.publish_data.landSize
    var bedroom = info.publish_data.bedrooms
    var bathrooms = info.publish_data.bathrooms
    var halfBathrooms = info.publish_data.halfBathrooms
    var parkingLots = info.publish_data.parkingLots
    var antiqueness = info.publish_data.antiqueness
    var floors = info.publish_data.floors
    var gardenSize = info.publish_data.gardenSize
    var price = info.publish_data.price
    var phone = info.publish_data.phone
    var resulPrice = parseInt(price,10);

    console.log("info",info);
    this.setState({
      uploadedFile: this.state.uploadedFile,
      publish_data : {
        suburb: info.publish_data.suburb,
        propertyType: info.publish_data.propertyType,
        operation: info.publish_data.operation,
        propertyState: info.values.stateRes,
        municipality: info.values.municipalityRes,
        street: info.publish_data.street,
        pc: info.publish_data.pc,
        externalNumber: info.publish_data.externalNumber,
        internalNumber: info.publish_data.internalNumber,
        externalNumberVisibility: info.publish_data.externalNumberVisibility,
        internalNumberVisibility: info.publish_data.internalNumberVisibility,
        landSize:  parseInt(terreno,10),
        landSizeUnit: info.publish_data.landSizeUnit,
        bedrooms:  parseInt(bedroom,10),
        bathrooms: parseInt(bathrooms,10),
        halfBathrooms: parseInt(halfBathrooms,10),
        parkingLots: parseInt(parkingLots,10),
        antiqueness: parseInt(antiqueness,10),
        floors: parseInt(floors,10),
        gardenSize: parseInt(gardenSize,10),
        gardenSizeUnit: info.publish_data.gardenSizeUnit,
        constructionSize: parseInt(construc,10),
        constructionSizeUnit: info.publish_data.constructionSizeUnit,
        air: info.publish_data.air,
        elevator: info.publish_data.elevator,
        gym: info.publish_data.gym,
        heat: info.publish_data.heat,
        laundry: info.publish_data.laundry,
        swimmingPool: info.publish_data.swimmingPool,
        security: info.publish_data.security,
        roofgarden: info.publish_data.roofgarden,
        price : resulPrice,
        phone: parseInt(phone,10),
        mail: info.publish_data.mail,
        description: info.publish_data.description,
        lat: info.publish_data.lat,
        lng: info.publish_data.lng,
        priceUnit: info.publish_data.priceUnit,
        priceType:info.publish_data.priceType,
        imagesUrls: this.state.uploadedFileCloudinaryUrl,
        propertyId:this.state.propertyId,
        pageSource:this.state.publish_data.pageSource
      }
    }, () =>{
        if(this.state.publish_data.imagesUrls.length >= 4){
          api.publishProperty(this.state.publish_data)
             .then((response) => {
               if(response.message === "property inserted"){
                 $(".static--form-success").fadeIn();
               }else if( !response.message ){
                 alert("tuvimos un problema intenta mas tarde")
               }
            });
        }else{
          this.setState({
            hasErrorsImage: true
          });
        }
      }
    )
  }


  handleChange( data, event ) {
    var state = this.state;
    state.publish_data[ data ] = event.target.value;

    if( this.state.publish_data.mail !== ""){
      this.emailValidate();
    }else{
      $("#emailValid").fadeOut()
    }

    this.setState( state );
      var propertyState = this.state.publish_data.propertyState;
      if(propertyState){
        api.municipalities(propertyState)
          .then((response)=>{
            this.setState({
              responseMun : response
            });
          })
      }

      var municipio = this.state.publish_data.municipality;
      var propertyStateMun = this.state.publish_data.propertyState;
      if( municipio && propertyStateMun ){
        api.neighbothoodCp(propertyStateMun,municipio)
        .then((response)=>{
          this.setState({
            responseNeig : response
          });
        })
      }


  }
  isNumberKey(evt){
      $(document).ready(function (){
        $('.solo-numero').keyup(function (){
          this.value = (this.value + '').replace(/[^0-9.]/g, '');
        });
      });
  }
  modal(){
    var info = this.state;

    var isStateEmpty = !info.publish_data.propertyType ||
      !info.publish_data.operation ||
      !info.publish_data.propertyState ||
      !info.publish_data.municipality ||
      !info.publish_data.suburb ||
      !info.publish_data.pc ||
      !info.publish_data.street ||
      !info.publish_data.constructionSize ||
      !info.publish_data.landSize ||
      !info.publish_data.antiqueness ||
      !info.publish_data.lat ||
      !info.publish_data.lng ||
      !info.publish_data.price ||
      !info.publish_data.phone ||
      !info.publish_data.mail ||
      !info.publish_data.description;
    // if(!isStateEmpty === false){
    //   alert("tienes que llenar todos los campos")
    // }
    if(!isStateEmpty){
      if(!info.publish_data.lat && !info.publish_data.lng  === true){
        $(".modal-error-lat").fadeIn();
      }
      var someName = JSON.stringify(this.state.publish_data);
      var propertyId1 = uuidv5(someName, '1b671a64-40d5-491e-99b0-da01ff1f3341');
      this.setState({
        propertyId: propertyId1
      });
      $(".static--form-images").fadeIn();
    }else{
      this.setState({
        hasErrors: true
      });
    }
  }
  handleInputChangeUnit(e, val, type) {
    var valu;
    if(e.target.type === 'checkbox'){
        if(e.target.checked)
          valu = "m2";
        else
          valu = "Ha";
        this.setState({
          publish_data: update(this.state.publish_data, {[val]: {$set: valu}})
        }
      );
    }
  }
  handleInputChangePrice(e, val, type) {
    var valu;
    if(e.target.type === 'checkbox'){
        if(e.target.checked)
          valu = "MXN";
        else
          valu = "USD";
        this.setState({
          publish_data: update(this.state.publish_data, {[val]: {$set: valu}})
        }
      );
    }
  }

  handleInputChange(e, val, type) {
      var valu;
      if(e.target.type === 'checkbox'){
        if(e.target.checked)
          valu = true;
        else
          valu = false;
        this.setState({
          publish_data: update(this.state.publish_data, {[val]: {$set: valu}})
        }
      );
    }
  }

  deleteImage(data){
    var namePath = data;
    var propertyId = this.propertyId
    api.deleteImage(namePath , propertyId)
      .then((response)=>{
        $(".name-images").fadeOut();
      })
 }

  reset() {
    document.getElementById("myForm").reset();
    document.getElementById("myForm").reset();
    $(".static--form-images").fadeOut();
    $(".static--form-success").fadeOut();
    this.setState({
      hasErrors: false,
      hasErrorImages:false,
      uploadedFile: null,
      dataMuni:{},
      uploadedFileCloudinaryUrl: [],
      NameURL:[],
      isChecked: false,
      publish_data: {
        suburb: "",
        propertyType: "",
        operation: "",
        propertyState: "",
        municipality: "",
        street: "",
        pc: "",
        externalNumber: "",
        internalNumber: "",
        externalNumberVisibility: true,
        internalNumberVisibility: false,
        landSize: '',
        landSizeUnit: "m2",
        bedrooms: 5,
        bathrooms: 5,
        halfBathrooms: 5,
        parkingLots: 5,
        antiqueness: 50,
        floors: 5,
        gardenSize: 50,
        gardenSizeUnit: "m2",
        constructionSize: '',
        constructionSizeUnit: "m2",
        air: false,
        elevator: false,
        gym: false,
        heat: false,
        laundry: false,
        swimmingPool: false,
        security: false,
        roofgarden: false,
        price : '',
        phone: '',
        mail: '',
        description: '',
        lat: '',
        lng: '',
        priceUnit:'MXN',
        checked: false,
        priceType:true,
        propertyId:'',
        imagesUrls:[],
        pageSource:"Klugger"
      }
    })
  }
  emailValidate() {
    var m = document.getElementById("email").value;
    var expreg = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if(expreg.test(m)){
      $("#emailValid").fadeOut()
    }else{
      if(this.state.publish_data.mail !== " "){
        $("#emailValid").fadeIn();
      }
    }
  }

  render() {

    var estados = [
      {id:"1", name:"Aguascalientes"},
      {id:"2", name:"Baja California"},
      {id:"3", name:"Baja California Sur"},
      {id:"4", name:"Campeche"},
      {id:"5", name:"Coahuila de Zaragoza"},
      {id:"6", name:"Colima"},
      {id:"7", name:"Chiapas"},
      {id:"8", name:"Chihuahua"},
      {id:"9", name:"CDMX"},
      {id:"10", name:"Durango"},
      {id:"11", name:"Guanajuato"},
      {id:"12", name:"Guerrero"},
      {id:"13", name:"Hidalgo"},
      {id:"14", name:"Jalisco"},
      {id:"15", name:"México"},
      {id:"16", name:"Michoacan de Ocampo"},
      {id:"17", name:"Morelos"},
      {id:"18", name:"Nayarit"},
      {id:"19", name:"Nuevo León"},
      {id:"20", name:"Oaxaca"},
      {id:"21", name:"Puebla"},
      {id:"22", name:"Querétaro"},
      {id:"23", name:"Quintana Roo"},
      {id:"24", name:"San Luis Potosi"},
      {id:"25", name:"Sinaloa"},
      {id:"26", name:"Sonora"},
      {id:"27", name:"Tabasco"},
      {id:"28", name:"Tamaulipas"},
      {id:"29", name:"Tlaxcala"},
      {id:"30", name:"Veracruz de ignacion de la llave"},
      {id:"31", name:"Yucatan"},
      {id:"32", name:"Zacatecas"}
    ]
    var state = this.state.publish_data.propertyState

    var resultadoEstadoArray = estados.find( estadoName => estadoName.id === state);

    if(resultadoEstadoArray !== ""){
      var resultadoEstado = resultadoEstadoArray ? resultadoEstadoArray.name :[];
    }
    var suburb = this.state.publish_data.suburb ? this.state.publish_data.suburb : [];
    var municipioArray = this.state.publish_data.municipality ? this.state.publish_data.municipality : [];

    var responseApi = this.state.responseMun ? this.state.responseMun : [];
    var resultadoMunicipio = responseApi.find( municipio => municipio.municipioId === municipioArray)

    if(resultadoMunicipio !== ""){
      var resultadoMunicipioApi = resultadoMunicipio ? resultadoMunicipio.municipio :[];
    }

    var municipios = responseApi && responseApi.length > 0 ?_.map( responseApi, function( data, i ){
      return( <option key={ i } value={data.municipioId}>{data.municipio}</option>);
    }) : '';

    var responseApiNeig = this.state.responseNeig ? this.state.responseNeig : [];
    var resultado = responseApiNeig.find( colon => colon.colonia === suburb );
    if(resultado !== undefined){
        var resultadoCp = resultado.cp ? resultado.cp : [];

        this.state.publish_data.pc = resultadoCp
    }
    var neighborhood = responseApiNeig && responseApiNeig.length > 0 ?_.map( responseApiNeig, function( data, i ){
      return( <option key={ i } value={data.colonia} >{data.colonia}</option>);
    }) : '';

    if(resultadoEstadoArray !== undefined && resultadoMunicipio !== undefined ){
      this.state.values.stateRes = resultadoEstadoArray.name;
      this.state.values.municipalityRes = resultadoMunicipio.municipio;
      //this.setArray(resultadoEstadoArray,resultadoMunicipio);
    }

    //parametros listos
    var str = this.spaceReplaceMap(this.state.publish_data.street)
    var pc = this.state.publish_data.pc
    var subur = this.spaceReplaceMap(this.state.publish_data.suburb)
    var externalNumber = this.state.publish_data.externalNumber

    var map = "https://www.google.com/maps/embed/v1/search?q=" + str + "%20" + externalNumber + "%20" + subur + "%20" + resultadoMunicipioApi + "%20" + pc + "%20" + resultadoEstado +"&key=AIzaSyDE1ClcNrmXPnRk1nIhCaIzZKQ_3lSQCRU";
    if(this.state.publish_data.street && this.state.publish_data.externalNumber && this.state.publish_data.suburb && resultadoMunicipioApi && resultadoEstado && this.state.publish_data.pc){
      this.geocode(window.google);
    }

    if(this.state.hasErrors){
      var error = 'Este campo es obligatorio';

      if(!this.state.publish_data.propertyType) var errorType = error;
      if(!this.state.publish_data.operation) var errorOper = error;
      if(!this.state.publish_data.propertyState) var errorState = error;
      if(!this.state.publish_data.municipality) var errorMuni = error;
      if(!this.state.publish_data.suburb) var errorSuburb = error;
      if(!this.state.publish_data.pc) var errorPc = error;
      if(!this.state.publish_data.street) var errorStreet = error;
      if(!this.state.publish_data.constructionSize) var errorConsSize = error;
      if(!this.state.publish_data.landSize) var errorLandSize = error;
      if(!this.state.publish_data.price) var errorPrice = error;
      if(!this.state.publish_data.phone) var errorPhone = error;
      if(!this.state.publish_data.mail) var errorMail = error;
      if(!this.state.publish_data.description) var errorDesc = error;

    }
    if(this.state.hasErrorsImage === true){
      var errorImg = 'Debes tener al menos 4 imagenes para publicar tu propiedad';
      var errorImages = errorImg;
    }
    var self = this;
    var nameImage = this.state.NameURL;

    var imagesUploaded = self.state.uploadedFileCloudinaryUrl;
    if(imagesUploaded.length > 0){

      imagesUploaded = imagesUploaded.toString();
      var path = imagesUploaded.split( '/' );
       var namePath = path.pop();
      //var final de key
      var key = namePath;

      var nameImages = nameImage && nameImage.length > 0 ?_.map( nameImage, function( data, i ){
console.log("data",data);
        var state = self.state;
        return(
          <article className="name-images" key={i}>
            <span>{data} </span>
            <span onClick={self.deleteImage.bind(state,key.toString())} style={{'cursor':'pointer'}}><p className="far fa-times-circle"></p></span>
          </article>
        );
      }) : '';
    }

    return (
      (this.state.logged===false)?
      <Redirect to="/" />:
      <section className="inmueble">
        <Header/>
        <form id="myForm">
          <h1>Selecciona <b>la ubicación de tu inmueble</b></h1>
            <div className="inmueble-div">
            <div className="selectCaracteristicas" >

              <article>
                <select className="borde" name="type" error={errorType} validate="true"
                  id="tipoPropiedad" required
                  value={this.state.publish_data.propertyType}
                    onChange={this.handleChange.bind(this,'propertyType')}>
                  <option>Tipo de Propiedad</option>
                  <option value="Casa">Casa</option>
                  <option value="Casa en condominio">Casa en condominio</option>
                  <option value="Departamento">Departamento</option>
                </select>
                <span className="errorOn">{errorType}</span>
              </article>
              <article>
                <select className="borde" name="operation" error={errorOper}
                  id="operation"
                  value={this.state.publish_data.operation}
                  onChange={this.handleChange.bind(this,'operation')}>
                  <option>Tipo de operación</option>
                  <option value="Venta">Venta</option>
                  <option value="Renta">Renta</option>
                </select>
                <span className="errorOn">{errorOper}</span>
              </article>
              <article>
                <select className="borde" name="state" error={errorState}
                  id="state"
                  value={this.state.publish_data.propertyState}
                  onChange={this.handleChange.bind(this,'propertyState')}>
                  <option>Estado</option>
                  <option value="1">Aguascalientes</option>
                  <option value="2">Baja California</option>
                  <option value="3">Baja California Sur</option>
                  <option value="4">Campeche</option>
                  <option value="5">Coahuila de Zaragoza</option>
                  <option value="6">Colima</option>
                  <option value="7">Chiapas</option>
                  <option value="8">Chihuahua</option>
                  <option value="9">CDMX</option>
                  <option value="10">Durango</option>
                  <option value="11">Guanajuato</option>
                  <option value="12">Guerrero</option>
                  <option value="13">Hidalgo</option>
                  <option value="14">Jalisco</option>
                  <option value="15">México</option>
                  <option value="16">Michoacan de Ocampo</option>
                  <option value="17">Morelos</option>
                  <option value="18">Nayarit</option>
                  <option value="19">Nuevo León</option>
                  <option value="20">Oaxaca</option>
                  <option value="21">Puebla</option>
                  <option value="22">Querétaro</option>
                  <option value="23">Quintana Roo</option>
                  <option value="24">San Luis Potosi</option>
                  <option value="25">Sinaloa</option>
                  <option value="26">Sonora</option>
                  <option value="27">Tabasco</option>
                  <option value="28">Tamaulipas</option>
                  <option value="29">Tlaxcala</option>
                  <option value="30">Veracruz de ignacion de la llave</option>
                  <option value="31">Yucatan</option>
                  <option value="32">Zacatecas</option>
                </select>
                <span className="errorOn">{errorState}</span>
              </article>
              <article>
                <select className="borde" name="municipality" error={errorMuni}
                  id="municipality"
                  value={this.state.publish_data.municipality}
                  onChange={this.handleChange.bind(this,'municipality')}>
                  <option>Municipio o delegación</option>
                  {municipios}
                </select>
                <span className="errorOn">{errorMuni}</span>
              </article>
              <article>
                <select className="borde" name="suburb" error={errorSuburb}
                  id="suburb"
                  value={this.state.publish_data.suburb}
                  onChange={this.handleChange.bind(this,'suburb')}>
                  <option>Colonia</option>
                  {neighborhood}
                </select>
                <span className="errorOn">{errorSuburb}</span>
              </article>
            </div>

            <section className="datos">
              <aside>
                <div>
                  <p className="publica_inmueble-datos-p">Código Postal</p>
                  <input type="text" className="publica_inmueble-datos-input"
                          maxLength="5"
                          name="pc"
                          id="pc"
                          disabled
                          placeholder={resultadoCp}
                          error={errorPc}
                          defaultValue={this.state.publish_data.pc}
                          onChange={this.handleChange.bind(this)}
                          />

                          <span className="errorOn">{errorPc}</span>
                </div>
                <div className="two">
                  <p className="publica_inmueble-datos-p">Calle</p>
                  <input type="text"
                          className="publica_inmueble-datos-input"
                          error={errorStreet}
                          maxLength="150"
                          name="street"
                          id="street"
                          defaultValue={this.state.publish_data.street}
                          onChange={this.handleChange.bind(this,'street')}/>
                  <span className="errorOn">{errorStreet}</span>
                </div>

                <div className="three">
                  <p className="publica_inmueble-datos-p">Número exterior</p>
                  <input type="text"
                          className="publica_inmueble-datos-input"
                          maxLength="150"
                          name="externalNumber"
                          id="externalNumber"
                          defaultValue={this.state.publish_data.externalNumber}
                          onChange={this.handleChange.bind(this,'externalNumber')}/>
                  <label className="switch">
                    <input  name='externalNumberVisibility' type="checkbox" defaultChecked label='externalNumberVisibility' onChange={(e, value) => {this.handleInputChange(e, 'externalNumberVisibility', 'checkbox', value)}}/>
                    <div className="slider round" />
                  </label>
                </div>
                <div className="four">
                <p className="publica_inmueble-datos-p">Número interior</p>
                  <input type="text"
                          className="publica_inmueble-datos-input"
                          maxLength="150"
                          name="internalNumber"
                          id="internalNumber"
                          defaultValue={this.state.publish_data.internalNumber}
                          onChange={this.handleChange.bind(this,'internalNumber')}/>
                  <label className="switch">
                    <input  name='internalNumberVisibility' type="checkbox" value='0' label='internalNumberVisibility' onChange={(e, value) => {this.handleInputChange(e, 'internalNumberVisibility', 'checkbox', value)}}/>
                    <div className="slider round" />
                  </label>
                </div>
              </aside>
            </section>
            <div className="content">
              <div className="borde-caracteristicas-map">
                <iframe id="iframe_id" title="mapa" src={map} allowFullScreen frameBorder={0} />
              </div>
              <div className="borde-caracteristicas">
                <div className="borde-caracteristicas-div">
                  <p>Selecciona las <b>caracteristicas de las que dispone tu inmueble</b></p>
                  <aside>
                    <p>Construcción</p>
                    <input type="text" error={errorConsSize}
                            maxLength="150"
                            name="constructionSize"
                            id="constructionSize"
                            className="solo-numero"
                            onKeyPress={ this.isNumberKey }
                            defaultValue={this.state.publish_data.constructionSize}
                            onChange={this.handleChange.bind(this,'constructionSize')}/>

                    <label className="switch">
                      <input  name='constructionSizeUnit' type="checkbox" defaultChecked label='constructionSizeUnit' onChange={(e, value) => {this.handleInputChangeUnit(e, 'constructionSizeUnit', 'checkbox', value)}}/>
                      <article className="slider round" />
                    </label>
                    <span className="errorOn">{errorConsSize}</span>
                  </aside>

                  <aside>
                    <p>Terreno</p>
                    <input type="text" error={errorLandSize}
                            maxLength="150"
                            name="landSize"
                            id="landSize"
                            className="solo-numero"
                            onKeyPress={ this.isNumberKey }
                            defaultValue={this.state.publish_data.landSize}
                            onChange={this.handleChange.bind(this,'landSize')} />

                    <label className="switch two">
                      <input  name='gardenSizeUnit' type="checkbox" defaultChecked label='gardenSizeUnit' onChange={(e, value) => {this.handleInputChangeUnit(e, 'gardenSizeUnit', 'checkbox', value)}}/>
                      <article className="slider round" />
                    </label>
                    <span className="errorOn">{errorLandSize}</span>
                  </aside>
                  <article>
                    <p>Recamara</p>
                    <InputRange
                      maxValue={10}
                      minValue={0}
                      step={1}
                      value={this.state.publish_data.bedrooms}
                      onChange={bedrooms => this.setState({ publish_data: update(this.state.publish_data, {
                        bedrooms: {$set: bedrooms}
                      })
                    })}
                      />
                  </article>
                  <article>
                    <p>Baños</p>
                    <InputRange
                      maxValue={10}
                      minValue={0}
                      step={1}
                      value={this.state.publish_data.bathrooms}
                      onChange={bathrooms => this.setState({ publish_data: update(this.state.publish_data, {
                        bathrooms: {$set: bathrooms}
                      })
                    })}
                      />
                  </article>
                  <article>
                    <p>Medios Baños</p>
                    <InputRange
                      maxValue={10}
                      minValue={0}
                      step={1}
                      value={this.state.publish_data.halfBathrooms}
                      onChange={halfBathrooms => this.setState({ publish_data: update(this.state.publish_data, {
                        halfBathrooms: {$set: halfBathrooms}
                      })
                    })}
                      />
                  </article>
                  <article>
                    <p>Estacionamiento</p>
                    <InputRange
                      maxValue={10}
                      minValue={0}
                      step={1}
                      value={this.state.publish_data.parkingLots}
                      onChange={parkingLots => this.setState({ publish_data: update(this.state.publish_data, {
                        parkingLots: {$set: parkingLots}
                      })
                    })}
                      />
                  </article>
                  <article>
                    <p>Antig&uuml;edad</p>
                    <InputRange
                      maxValue={100}
                      minValue={0}
                      step={5}
                      value={this.state.publish_data.antiqueness}
                      onChange={antiqueness => this.setState({ publish_data: update(this.state.publish_data, {
                        antiqueness: {$set: antiqueness}
                      })
                    })}
                      />
                  </article>
                  <article>
                    <p>Número de niveles</p>
                    <InputRange
                      maxValue={10}
                      minValue={0}
                      step={1}
                      value={this.state.publish_data.floors}
                      onChange={floors => this.setState({ publish_data: update(this.state.publish_data, {
                        floors: {$set: floors}
                      })
                    })}
                      />
                  </article>
                  <article className="article">
                    <p>M2 de jardín</p>
                    <InputRange
                      maxValue={100}
                      minValue={0}
                      step={5}
                      value={this.state.publish_data.gardenSize}
                      onChange={gardenSize => this.setState({ publish_data: update(this.state.publish_data, {
                        gardenSize: {$set: gardenSize}
                      })
                    })}
                      />
                  </article>

                  <button> + </button>
                </div>
              </div>
            </div>
            <section className="content2">
              <div className="caracteristicas-two">
                <p>Selecciona los <b>servicios de los que dispone tu inmueble</b></p>
                <label>
                  <input  name='air' type="checkbox" value='0' label='air' onChange={(e, value) => {this.handleInputChange(e, 'air', 'checkbox', value)}}/>Aire acondicionado
                </label>
                <label>
                  <input  name='elevator' type="checkbox" value='0' label='elevator' onChange={(e, value) => {this.handleInputChange(e, 'elevator', 'checkbox', value)}}/>Elevador
                </label>
                <label>
                  <input  name='gym' type="checkbox" value='0' label='gym' onChange={(e, value) => {this.handleInputChange(e, 'gym', 'checkbox', value)}}/>Gimnasio
                </label>
                <label>
                  <input  name='heat' type="checkbox" value='0' label='heat' onChange={(e, value) => {this.handleInputChange(e, 'heat', 'checkbox', value)}}/>Calefacción
                </label>
                <label>
                  <input  name='laundry' type="checkbox" value='0' label='laundry' onChange={(e, value) => {this.handleInputChange(e, 'laundry', 'checkbox', value)}}/>Lavandería
                </label>
                <label>
                  <input  name='swimmingPool' type="checkbox" value='0' label='swimmingPool' onChange={(e, value) => {this.handleInputChange(e, 'swimmingPool', 'checkbox', value)}}/>Alberca
                </label>
                <label>
                  <input  name='security' type="checkbox" value='0' label='security' onChange={(e, value) => {this.handleInputChange(e, 'security', 'checkbox', value)}}/>Seguridad privada
                </label>
                <label>
                  <input  name='roofgarden' type="checkbox" value='0' label='roofgarden' onChange={(e, value) => {this.handleInputChange(e, 'roofgarden', 'checkbox', value)}}/>Roof Garden
                </label>
                <button type="submit"> + </button>
              </div>
              <div className="borde-caracteristicas-desc">
                <div>
                  <p>Describa el inmueble</p>
                  <textarea onChange={this.handleChange.bind(this,'description')} error={errorDesc}
                            maxLength={250} />
                            <span className="errorOn">{errorDesc}</span>
                  <div>
                    <p>Ingresa tu información de contacto</p>
                    <p>Indica el precio  </p>
                    <div className="contact">
                      <article>
                        <span>Teléfono:<input type="text" error={errorPhone}
                                    name="phone"
                                    id="phone"
                                    className="solo-numero"
                                    onKeyPress={ this.isNumberKey }
                                    defaultValue={this.state.publish_data.phone}
                                    onChange={this.handleChange.bind(this,'phone')}
                                    maxLength="10"/>
                        </span>
                        <span className="errorOn">{errorPhone}</span>
                      </article>
                      <article>
                        <span>Correo electrónico:<input type="email" error={errorMail} required="required"
                                    name="mail"
                                    id="email"
                                    defaultValue={this.state.publish_data.mail}
                                    onChange={this.handleChange.bind(this,'mail')}
                                    maxLength="150"/>
                        </span>
                        <span className="errorOn">{errorMail}</span>
                        <div id="emailValid">Correo no válido</div>
                      </article>
                    </div>
                    <div className="price">
                      <label className="switch">
                      <input  name='priceUnit' type="checkbox" defaultChecked label='priceUnit' onChange={(e, value) => {this.handleInputChangePrice(e, 'priceUnit', 'checkbox', value)}}/>
                        <div className="slider round" />
                      </label>
                      <span>Precio:<input type="text" error={errorPrice}
                                  name="price"
                                  id="price"
                                  defaultValue={this.state.publish_data.price}
                                  className="solo-numero"
                                  onKeyPress={ this.isNumberKey }
                                  onChange={this.handleChange.bind(this,'price')}
                                  maxLength="20"/>
                                </span>
                                <span className="errorOn">{errorPrice}</span>
                      <article className="priceType">
                        <span>¿El precio es negociable?</span>
                        <label className="switch">
                          <input  name='priceType' type="checkbox" defaultChecked label='priceType' onChange={(e, value) => {this.handleInputChange(e, 'priceType', 'checkbox', value)}}/>
                          <div className="slider round" />
                        </label>
                      </article>
                      <a onClick={this.modal.bind(this)}> Continuar con el ultimo paso </a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <aside className="static--form-images">
            <div className="static--form-images-content">
              <div className="images">
                <div className="FileUpload">
                    <div className="FileUpload">
                      <Dropzone
                        onDrop={this.onImageDrop.bind(this)}
                        multiple={false}
                        accept={".png,.jpg,.jpeg"}
                        >
                        <div>Coloque al menos 4 imagenes de su inmueble, haga clic para seleccionar un archivo.</div>
                      </Dropzone>
                      <span className="errorImages">{errorImages}</span>
                    </div>

                    <div className="nombreImagenes">
                      {this.state.uploadedFileCloudinaryUrl === '' ? null :
                      <div>
                        {nameImages}
                        {/*<img className="imagePreview" src={this.state.uploadedFileCloudinaryUrl} alt="" />*/}
                      </div>}

                    </div>
                </div>
                <button type="submit" className="submit"> + </button>
              </div>
              <a className="buttonPublicar" onClick={this.newPublishProperty.bind(this)}> Publicar </a>
              <p onClick={this.closeModal}style={{"zIndex":"99999999", "position": "relative", "textAlign": "right", "top": "-14em", "cursor":"pointer","fontWeight": "800","fontFamily": 'Open sans'}}>X</p>
            </div>
          </aside>
          <aside className="modal-error-lat">
            <div className="modal-error-lat-content">
              <div>
                <h1>¡Ups no pudimos encontrar la dirección que ingresaste, regresa para revisar!</h1>
                <a className="submit" onClick={this.closeModal}> Aceptar </a>
              </div>
            </div>
          </aside>
          <aside className="static--form-success">
            <div className="static--form-success-content">
                <h1>Tu propiedad ha sido guardada</h1>
                <button type="submit" onClick={this.reset}>Publicar otra propiedad</button>
            </div>
          </aside>
        </form>
        <Footer/>
      </section>
    );
  }
}

export default Publica;
