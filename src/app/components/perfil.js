import React from 'react';
import perfilGnd from '../../media/perfil_gnd.png';
import linea from '../../media/linea_division.png';
import { Link } from 'react-router';
export class Perfil extends React.Component {


  render() {
    return (
      <section className="sectionInfo border">
        <div>
          <a className="active">Mi Perfil</a>
          <Link to="/propiedades_publicadas" className="border">Propiedades Publicadas</Link>
          <a className="border">Propiedades Guardadas</a>
          <Link to="/publica_inmueble" className="border">Publicar Propiedad</Link>
        </div>
        <section>
          <img src={perfilGnd} alt="" />
          <button>Editar foto</button>
        </section>
        <section className="configuracion">
          <h6>Configuración de perfil</h6>
          <img src={linea} alt="" />
          <b>María</b>
          <img src={perfilGnd} alt="" className="profile"/>
          <article>
            <span>Nombre:</span>
            <input type="text"/>
          </article>
          <article>
            <span>Apellidos:</span>
            <input type="text"/>
          </article>
          <article>
            <span>Correo:</span>
            <input type="email"/>
          </article>
          <article>
            <span>Tipo de usuario:</span>
            <select className="borde" name="type"
              id="tipoUsuario">
              <option>Seleccione</option>
              <option value="propietario">Propietario</option>
              <option value="inmobiliaria">Inmobiliaria</option>
            </select>
          </article>
          <article>
            <span>Contraseña:</span>
            <input type="password"/>
          </article>
          <article>
            <span>Confirmar contraseña:</span>
            <input type="password"/>
          </article>
        </section>
        <section className="contacto">
          <h6>Información de contacto de tus propiedades</h6>
          <img src={linea} alt="" />
          <article>
            <span>Teléfono:</span>
            <input type="text"/>
          </article>
          <article>
            <span>Correo:</span>
            <input type="email"/>
          </article>
          <div>
            <a>GUARDAR CAMBIOS</a>
            <a className="salir">SALIR</a>
          </div>
        </section>
      </section>
    );
  }
}

export default Perfil;
