import React from 'react';
import $ from 'jquery';
import _ from 'lodash';
import { Link } from 'react-router';
import roomsImg from '../../media/icono1_phone.png';
import bathroomsImg from '../../media/icono2_phone.png';
import parkingLotsImg from '../../media/icono3_phone.png';
import constrImg from '../../media/icono4_phone.png';
import logoKlugger from '../../media/logo_phone.png';
import Autocomplete from 'react-google-autocomplete';
//import InputRange from 'react-input-range';
import update from 'immutability-helper';
//mapa
import GoogleMapReact from 'google-map-react';
import img_src from '../../media/point-green.png';
import point from '../../media/point-green.png';

//api
import api from'../utils/api';
import { browserHistory } from 'react-router';
//url imagenes
const AnyReactComponent = ({ img_src }) => <div><img src={point} className="markers" alt="punto"/></div>;

const OPTIONS = {
  minZoom: 8
}

export class Catalogo extends React.Component {
  static defaultProps = {
    zoom: 12
  };

  constructor(props){
    super(props);

    this.state = {
      pageSource: [],
      responseApi: {},
      img_src: img_src,
      bounds:"",
      filters:false,
      centro: {
        lat: "",
        lng: ""
      },
      e:false,
      cargado: false,
      resultado: {},
      surfaceValue: {
        min: 0,
        max: 0,
      },
      roomsValue: {
        min: 0,
        max: 0,
      },
      bathroomsValue: {
        min: 0,
        max: 0,
      },
      priceValue: {
        min: 0,
        max: 0,
      },
      parkingLotsValue: {
        min: 0,
        max: 0,
      },
      form_catalog:{
        bathroomsMin:0,
        bathroomsMax:1000000,
        bedroomsMin:0,
        bedroomsMax:10000000,
        constructionSizeMin:0,
        constructionSizeMax:1000,
        parkingLotsMin:0,
        parkingLotsMax:1000,
        priceMin:0,
        priceMax:100000000,
        propertyType:"departamento",
        operationType:"renta",
        latMin:19.3910038,
        latMax:19.4326077,
        lngMin:-99.133208,
        lngMax:-99.2836986

      }
    }
  }
  componentDidUpdate(prevState) {
    // Typical usage (don't forget to compare props):
    // if (this.props.userID !== prevProps.userID) {
    //   this.fetchData(this.props.userID);
    // }
    console.log("prevState",prevState);
  }
  componentDidMount(){
    console.log("this.props.location.state",this.props.location.state);
    if(this.props.location.state !== undefined){
      console.log("props !==");
      this.setState({
        surfaceValue: {
          min: 0,
          max: this.props.location.state.props.maximumConstructionSize,
        },
        roomsValue: {
          min: 0,
          max: this.props.location.state.props.maximumBedrooms,
        },
        bathroomsValue: {
          min: 0,
          max: this.props.location.state.props.maximumBathrooms,
        },
        priceValue: {
          min: 0,
          max: this.props.location.state.props.maximumPrice,
        },
        parkingLotsValue: {
          min: 0,
          max: this.props.location.state.props.maximumParkingLots,
        },
        form_catalog : update(this.state.form_catalog, {
          priceMax: {$set: this.props.location.state.props.maximumPrice},
          bedroomsMax: {$set: this.props.location.state.props.maximumBedrooms},
          bathroomsMax: {$set: this.props.location.state.props.maximumBathrooms},
          parkingLotsMax: {$set: this.props.location.state.props.maximumParkingLots},
          constructionSizeMax: {$set: this.props.location.state.props.maximumConstructionSize},
          latCenter: {$set: this.props.location.state.props.latCenter},
          lngCenter: {$set: this.props.location.state.props.lngCenter},
          latMax: {$set: this.props.location.state.params.latMax},
          latMin: {$set: this.props.location.state.params.latMin},
          lngMax: {$set: this.props.location.state.params.lngMax},
          lngMin: {$set: this.props.location.state.params.lngMin},
        })
      });
    }
    this.setState({
      form_catalog: update(this.state.form_catalog, {
        operationType: {$set: this.props.location.state ? this.props.location.state.operationType : "renta"},
        propertyType: {$set: this.props.location.state ? this.props.location.state.propertyType : "departamento"}
      })
    }, () => {
      if(Object.keys(this.state.responseApi).length!==0){
        var lat = this.state.responseApi ? this.state.responseApi.latCenter: 19.4326018;
        var lng = this.state.responseApi ? this.state.responseApi.lngCenter: -99.1332049;
        this.setState({
          centro: {lat,lng}
        });

      }
      else{
        lat = this.props.location.state ? this.props.location.state.props.latCenter : 19.4326018;
        lng = this.props.location.state ? this.props.location.state.props.lngCenter : -99.1332049;

        this.setState({
          centro: {lat,lng}
        }, () => {
          if(!this.props.location.state){
            $('.loading').removeClass("loadingDesactive")
            $('.loading').addClass("loadingActive");
            api.rect(this.state.form_catalog , this.state.form_catalog.latMin, this.state.form_catalog.latMax, this.state.form_catalog.lngMin, this.state.form_catalog.lngMax)
            .then((response)=>{

              if( response.properties.length !== 0 ){
                this.setState({
                  responseApi: response,
                  cargado: true
                }, () => $('.loading').addClass("loadingDesactive"))
              }
            })
          }

        });
      }
    });
   }




  handleChange = ( data, event ) => {
    var state = this.state;
    state.form_catalog[ data ] = event.target.value;

    var bathroomsMinInt=this.state.form_catalog.bathroomsMin,
    bathroomsMaxInt=this.state.form_catalog.bathroomsMax,
    bedroomsMinInt=this.state.form_catalog.bedroomsMin,
    bedroomsMaxInt=this.state.form_catalog.bedroomsMax,
    constructionSizeMinInt=this.state.form_catalog.constructionSizeMin,
    constructionSizeMaxInt=this.state.form_catalog.constructionSizeMax,
    parkingLotsMinInt=this.state.form_catalog.parkingLotsMin,
    parkingLotsMaxInt=this.state.form_catalog.parkingLotsMax,
    priceMinInt=this.state.form_catalog.priceMin,
    priceMaxInt=this.state.form_catalog.priceMax;

    this.setState({
      form_catalog: update(this.state.form_catalog, {
        bathroomsMin:{$set: parseInt(bathroomsMinInt,10)},
        bathroomsMax:{$set: parseInt(bathroomsMaxInt,10)},
        bedroomsMin:{$set: parseInt(bedroomsMinInt,10)},
        bedroomsMax: {$set: parseInt(bedroomsMaxInt,10)},
        constructionSizeMin: {$set: parseInt(constructionSizeMinInt,10)},
        constructionSizeMax: {$set: parseInt(constructionSizeMaxInt,10)},
        parkingLotsMin: {$set: parseInt(parkingLotsMinInt,10)},
        parkingLotsMax: {$set: parseInt(parkingLotsMaxInt,10)},
        priceMin: {$set: parseInt(priceMinInt,10)},
        priceMax: {$set: parseInt(priceMaxInt,10)}
      })
    }, () => {
    var obj = this.state.form_catalog;
    if(this.state.form_catalog.latMax !== "" && this.state.form_catalog.latMin !== "" && this.state.form_catalog.lngMax !== "" && this.state.form_catalog.lngMin !== ""){
      $('.loading').removeClass("loadingDesactive");
      $('.loading').addClass("loadingActive");
      api.rect(obj)
        .then((response)=> {
          this.setState({
            responseApi: response,
            cargado: true
          }, () => $('.loading').addClass("loadingDesactive"))
        })
      }
    });
  }

  switchClick(){

    $('#togBtn').change(function(){
     if($(this).attr('checked')){
          $(this).val('TRUE');
          var checked = $(this).val('TRUE')[0].checked;
          if( checked === false){
            $("#catalogo").fadeIn();
            $('.catalogo--filtros .filtros article').fadeOut();
          }
          else{
            $('.catalogo--filtros .filtros article').fadeIn();
            $("#catalogo").fadeOut();
          }
     }
    });
  }

  apiCatalog(data){
    $('.loading').removeClass("loadingDesactive")
    $('.loading').addClass("loadingActive");
    var latMin = data.f.b,
         latMax = data.f.f,
         lngMax = data.b.f,
         lngMin = data.b.b;
     this.setState({
       form_catalog: update(this.state.form_catalog, {
         latMin:{$set: latMin},
         lngMin:{$set: lngMin},
         latMax:{$set: latMax},
         lngMax:{$set: lngMax}
       })
     }, () => {
      var obj = this.state.form_catalog;
      api.rect(obj)
      .then((response)=>{
        //if(response.properties.length!==0){
          this.setState({
            responseApi: response,
            cargado: true
          }, () => {
              $('.loading').addClass("loadingDesactive");
              var centroResponseLat = this.state.responseApi.latCenter;
              var centroResponseLng = this.state.responseApi.lngCenter;
              this.setState({
                centro: update(this.state.centro, {
                  lat: {$set: centroResponseLat},
                  lng: {$set: centroResponseLng}
                })
              })
            }
          )
      //  }
        // else {
        //   $('.loading').addClass("loadingDesactive");
        //   this.setState({
        //     centro: update(this.state.centro, {
        //       lat: {$set: 19.4326018},
        //       lng: {$set: -99.1332049}
        //     })
        //   })
        // }
      })
    });

  }

  detalleInmueble = (data,data2) => {
    $('.loading').removeClass("loadingDesactive")
    $('.loading').addClass("loadingActive");
    var propertyId = data;
    var hashKey = parseInt(data2,10);
    api.info( propertyId, hashKey )
      .then((response)=>{
        $('.loading').addClass("loadingDesactive");
        return response;
      }).then(function(rrr){
        if(rrr){
          browserHistory.push({
            pathname: "/detalle_inmueble/?propertyId=" + data +"&hashKey="+ hashKey,
            state: { props: rrr }
          })
        }
      })
  }

  onChildClick(key, props) {
    var propiedades = Object.keys(this.state.responseApi).length>0 ? this.state.responseApi.properties : this.props.location.state.props.properties;
    var resultado = propiedades.find( lat => lat.lat === props.lat );
    window.$(".inmueble--detalle-content").fadeIn();
    this.setState({
      resultado : resultado
    })
  }

  handleMapChange({ center, zoom, bounds }){
    var centroMap = center.lng;
    var locStateCenter = this.props.location.state ? this.props.location.state.props.lngCenter : -99.1332049;

    if(centroMap !== locStateCenter){
      var lat = this.state.centro.lat;
      var lng = this.state.centro.lng

      var latMax = bounds.nw.lat;
      var latMin = bounds.se.lat;
      var lngMax = bounds.se.lng;
      var lngMin = bounds.nw.lng;


      this.setState({
        centro: {lat, lng} ,
        form_catalog: update(this.state.form_catalog, {
          latMin : {$set: latMin},
          lngMin : {$set: lngMin},
          latMax : {$set: latMax},
          lngMax : {$set: lngMax}
        }),

      }, () => {
        $('.loading').removeClass("loadingDesactive")
        $('.loading').addClass("loadingActive");
        api.rect(this.state.form_catalog)
        .then((response)=>{
          this.setState({
            responseApi: response,
            cargado: true
          }, () => $('.loading').addClass("loadingDesactive"))
        })
      });

    }
  }

  closeModal(){
    $(".inmueble--detalle-content").fadeOut();
  }
  handleInputChange(e, val, type) {

      var reponseUrl = val.source;
      var arr = [];
      if( this.state.pageSource.length === 0 ){
          arr.push( reponseUrl );
          this.setState({
            pageSource: arr
          }, () => {
          api.rect(this.state.form_catalog)
            .then((response)=> {
              this.setState({
                responseApi: response,
                cargado: true
              }, () => $('.loading').addClass("loadingDesactive"))
            })
        }
        );
      }else{
          arr.push( reponseUrl );
          arr = arr.concat( this.state.pageSource );
          this.setState({
            pageSource: arr
          }
        );
      }
  }

  render() {
    var listItems = [];

    if(this.props.location.state && this.props.location.state.props.differentSources) {
      const numbers = this.props.location.state.props.differentSources;
      listItems = numbers.map((source) =>
        <article key={source} className="listSources" >
            <input  name='pageSource' type="checkbox" label='pageSource' onChange={(e, value) => {this.handleInputChange(e, {source}, 'checkbox',value)}}/>
            <span>{source}</span>
        </article>
      );
    }

    if(Object.keys(this.state.resultado).length !== 0){
      if(this.state.resultado.description){
        var description = <p>{this.state.resultado.description}</p>;
      }
      var modalPropiedad =
        <div className="inmueble--detalle-content border modal-catalogo">
            <p onClick={this.closeModal}>X</p>
            <img src={ this.state.resultado.imagesUrls[0] } alt="" />
            <p className="modal-catalogo--p">{this.state.resultado.price} </p>
            <p>{this.state.resultado.title}</p>
            <p>{this.state.resultado.municipality}</p>

            <div className="props-modal">
              <aside>
                <img src={roomsImg} alt="habitaciones"/>
                <span>{this.state.resultado.bedrooms}</span>
              </aside>
              <aside>
                <img src={bathroomsImg} alt="baños"/>
                <span>{this.state.resultado.bathrooms}</span>
              </aside>
              <aside>
                <img src={parkingLotsImg} alt="Estacionamiento"/>
                <span>{this.state.resultado.parkingLots}</span>
              </aside>
              <aside>
                <img src={constrImg} alt="construccion"/>
                <span>{this.state.resultado.constructionSize}</span>
              </aside>
            </div>
            {description}
            <Link to={ 'detalle_inmueble/?propertyId=' + this.state.resultado.propertyId + '&hashKey=' + this.state.resultado.hashKey } > LEER MÁS</Link>
        </div>
      ;
    }

    var response = this.props.location.state ? this.props.location.state.props.properties : [];
    if(this.state.responseApi !== undefined && Object.keys(this.state.responseApi).length!==0 && this.state.responseApi.properties){

      var article = this.state.responseApi.properties && this.state.responseApi.properties.length > 0 ?_.map( this.state.responseApi.properties, function( data, i ){
        var img = data.imagesUrls[0],
         mun = data.municipality,
         title = data.title,
         state = data.propertyState,
         price = data.price,
         rooms = data.bedrooms,
         bathrooms = data.bathrooms,
         parkingLots = data.parkingLots,
         constr = data.constructionSize;
        if( data.pageSource === "Klugger"){
          var logoSource = <img src={logoKlugger} alt="logo" style={{'width':'60%', 'display':'block'}}/>;
        }

        return(
          <aside key={ i + 1}>
            <h6>PROPIEDAD {i}</h6>
            <article>
              <img src={ img } alt="" />
              <p><b>{price}</b></p>
              <span>{title}</span>
              <span>{state}</span>
              <span>{mun}</span>
              <div className="props">
                <aside>
                  <img src={roomsImg} alt="habitaciones"/>
                  <span>{rooms}</span>
                </aside>
                <aside>
                  <img src={bathroomsImg} alt="baños"/>
                  <span>{bathrooms}</span>
                </aside>
                <aside>
                  <img src={parkingLotsImg} alt="Estacionamiento"/>
                  <span>{parkingLots}</span>
                </aside>
                <aside>
                  <img src={constrImg} alt="construccion"/>
                  <span>{constr}</span>
                </aside>
              </div>
              {logoSource}
              <Link to={ 'detalle_inmueble/?propertyId=' + data.propertyId + '&hashKey=' + data.hashKey } > LEER MÁS</Link>
            </article>
          </aside>
        );
      }) : '';
    }
    else if(this.state.responseApi === undefined || Object.keys(this.state.responseApi).length === 0 || !this.state.responseApi.properties){
        article = response && response.length > 0 ?_.map( response, function( data, i ){
        var mun = data.municipality;
        var title = data.title;
        var state = data.propertyState;
        var price = data.price;
        var rooms = data.bedrooms;
        var bathrooms = data.bathrooms;
        var parkingLots = data.parkingLots;
        var constr = data.constructionSize;
        var img = data.imagesUrls[0];
        if(data.pageSource === "Klugger"){
          var logoSource = <img src={logoKlugger} alt="logo"/>;
        }
        return(
          <aside key={ i+1 }>
            <h6>PROPIEDAD {i}</h6>
            <article>
              <img src={ img } alt="" />
              <p><b>{price}</b></p>
              <span>{title}</span>
              <span>{state}</span>
              <span>{mun}</span>
              <div className="props">
                <aside>
                  <img src={roomsImg} alt="habitaciones"/>
                  <span>{rooms}</span>
                </aside>
                <aside>
                  <img src={bathroomsImg} alt="baños"/>
                  <span>{bathrooms}</span>
                </aside>
                <aside>
                  <img src={parkingLotsImg} alt="Estacionamiento"/>
                  <span>{parkingLots}</span>
                </aside>
                <aside>
                  <img src={constrImg} alt="construccion"/>
                  <span>{constr}</span>
                </aside>
              </div>
              {logoSource}
              <Link to={ "detalle_inmueble/?propertyId=" + data.propertyId + "&hashKey=" + data.hashKey }  > LEER MÁS</Link>
            </article>
          </aside>
        );
      }) : '';
    }
    if(this.state.responseApi !== undefined && Object.keys(this.state.responseApi).length!==0 && this.state.responseApi.properties){
      var points = this.state.responseApi.properties && this.state.responseApi.properties.length > 0 ?_.map( this.state.responseApi.properties, function( data, i ){
        return(
            <AnyReactComponent
              key={i}
              lat={data.lat}
              lng={data.lng}
              img_src={img_src}
            />
          );
        }) : '';
    }else if(this.state.responseApi === undefined || Object.keys(this.state.responseApi).length === 0 || !this.state.responseApi.properties){
      points = response && response.length > 0 ?_.map( response, function( data, i ){
        return(
            <AnyReactComponent
              key={i}
              lat={data.lat}
              lng={data.lng}
              img_src={img_src}
              />
          );
        }) : '';
    }
   //  var priceMaxRange = this.props.location.state ? this.props.location.state.props.maximumPrice : 1000;
   // var bedroomsMaxRange = this.props.location.state ? this.props.location.state.props.maximumBedrooms : 1000;
   // var bathroomsMaxRange = this.props.location.state ? this.props.location.state.props.maximumBathrooms : 1000;
   // var parkingLotsMaxRange = this.props.location.state ? this.props.location.state.props.maximumParkingLots  : 100;
   // var constructionSizeMaxRange = this.props.location.state ? this.props.location.state.props.maximumConstructionSize : 1000;

    var centerL;

    if(this.state.centro.lng !== "" && this.state.centro.lat !== "")
      centerL = this.state.centro
    else if(this.state.centro.lng === "" && this.state.centro.lat === "" && this.props.location.state)
      centerL = {
        lat: this.props.location.state.props.latCenter,
        lng: this.props.location.state.props.lngCenter
      }
    else if(this.state.centro.lng === "" && this.state.centro.lat === "" && !this.props.location.state)
      centerL = {
        lat: 19.4326018,
        lng: -99.1332049
      }
    return (
      <section className="catalogo">
        <section>
          <h1>Catálogo <b>de propiedades</b></h1>
          <div className="fondo">
          <Autocomplete
            style={{width: '90%'}}
            onPlaceSelected={(place) => {
              var bounds = place.geometry.viewport ? place.geometry.viewport :[];
              if(bounds !== ""){
                this.apiCatalog(bounds)
              }
            }}
            types={['(regions)']}
          />
          </div>
        </section>
        <section className="catalogo--filtros">
          <div className="filtros">
            <label className="switch"><input type="checkbox" id="togBtn" defaultChecked onClick={this.switchClick.bind(this,'filters')}/><div className="slider round" /></label>
            <article >
              <b>Operación</b>
              <select name="operationType"
                      id="operationType"
                      value={this.state.form_catalog.operationType}
                      onChange={(e) => this.handleChange('operationType', e)}>

                <option value="Venta">Venta</option>
                <option value="Renta">Renta</option>
              </select>
            </article>
            <article>
              <b>Tipo de inmueble</b>
              <select name="propertyType"
                      id="propertyType"
                      value={this.state.form_catalog.propertyType}
                      onChange={(e) => this.handleChange('propertyType', e)}>

                <option value="Casa">Casa</option>
                <option value="Casa en condominio">Casa en Condominio</option>
                <option value="Departamento">Departamento</option>
              </select>
            </article>
            <article>
              <b>Precio</b>
              {/*<InputRange
                maxValue={priceMaxRange}
                step={1000}
                minValue={0}
                value={this.state.priceValue}
                onChange={priceValue => this.setState({ priceValue })}
                />*/}
                <div>
                  <input type="text" placeholder="Desde" onChange={(e) => this.handleChange('priceMin', e)}/><input type="text" placeholder="Hasta" onChange={(e) => this.handleChange('priceMax', e)}/>
                </div>
            </article>
            <article>
              <b>Superficie</b>
              {/*<InputRange
                maxValue={constructionSizeMaxRange}
                minValue={0}
                value={this.state.surfaceValue}
                onChange={surfaceValue => this.setState({ surfaceValue })} />*/}
                <div>
                  <input type="text" placeholder="Desde" onChange={(e) => this.handleChange('constructionSizeMin', e)}/><input type="text" placeholder="Hasta" onChange={(e) => this.handleChange('constructionSizeMax', e)}/>
                </div>
            </article>
            <article>
              <b>Habitaciones</b>
              {/*<InputRange
                maxValue={bedroomsMaxRange}
                minValue={0}
                value={this.state.roomsValue}
                onChange={roomsValue => this.setState({ roomsValue })} />*/}
                <div>
                  <input type="text" placeholder="Desde" onChange={(e) => this.handleChange('bedroomsMin', e)}/><input type="text" placeholder="Hasta" onChange={(e) => this.handleChange('bedroomsMax', e)}/>
                </div>
            </article>
            <article>
              <b>Baños</b>
              {/*<InputRange
                maxValue={bathroomsMaxRange}
                minValue={0}
                value={this.state.bathroomsValue}
                onChange={bathroomsValue => this.setState({ bathroomsValue })} />*/}
                <div>
                  <input type="text" placeholder="Desde" onChange={(e) => this.handleChange('bathroomsMin', e)}/><input type="text" placeholder="Hasta" onChange={(e) => this.handleChange('bathroomsMax', e)}/>
                </div>
            </article>
            <article>
              <b>Estacionamiento</b>
              {/*<InputRange
                maxValue={parkingLotsMaxRange}
                minValue={0}
                value={this.state.parkingLotsValue}
                onChange={parkingLotsValue => this.setState({ parkingLotsValue })} />*/}
                <div>
                  <input type="text" placeholder="Desde" onChange={(e) => this.handleChange('parkingLotsMin', e)}/><input type="text" placeholder="Hasta" onChange={(e) => this.handleChange('parkingLotsMax', e)}/>
                </div>
            </article>
            <b style={{'float':'left'}}>Fuentes de información</b>
            {listItems}
          </div>
          <div className="mapa">
            <div style={{ height: '50em', width: '100%', margin: '0 auto', position: 'relative' }}>
              <GoogleMapReact
                options = {OPTIONS}
                bootstrapURLKeys={{ key:'AIzaSyBjPqgfje7_Dg_qlZ9eiie7zLpw-9JN0SA' }}
                center={centerL}
                defaultZoom={this.props.zoom}
                onChildClick={this.onChildClick.bind(this)}
                onChange={this.handleMapChange.bind(this)}
              >
                {points}
              </GoogleMapReact>
            </div>
          </div>
          {modalPropiedad}
          <section className="catalogo-inmuebles" id="catalogo">
            <div className="catalogo--catalogo">
              {article}
            </div>
          </section>
        </section>
      </section>
    );
  }

}

export default Catalogo;
