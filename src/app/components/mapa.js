import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import img_src from '../../media/point-green.png';

const __GAPI_KEY__ = 'AIzaSyBjPqgfje7_Dg_qlZ9eiie7zLpw-9JN0SA';
const AnyReactComponent = ({ img_src }) => <div><img src={img_src} className="markers" alt="punto"/></div>;

export class Mapa extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        markers: [],

    }
  }
  static defaultProps = {
    center: {
      lat: 19.38615,
      lng: -99.3194397
    },
    zoom: 11
  };

  componentDidMount(){
    // or you can set markers list somewhere else
    // please also set your correct lat & lng
    // you may only use 1 image for all markers, if then, remove the img_src attribute ^^
    this.setState({
      markers: [{lat: 19.385042, lng: -99.1697917, img_src: img_src},{lat: 19.384253, lng: -99.1547597, img_src: img_src },{lat: 19.403391, lng: -99.1592177,  img_src: img_src}],
    });
  }

  onChildClick(key, props) {
    console.log('child click props',props);
  }


  render() {

    return (
      
      // Important! Always set the container height explicitly
      <div style={{ height: '50em', width: '50em', margin: '0 auto', top: '9em', position: 'relative' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key:'AIzaSyBjPqgfje7_Dg_qlZ9eiie7zLpw-9JN0SA' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
            onChildClick={this.onChildClick.bind(this)}
        >
        {this.state.markers.map((marker, i) =>{
            return(
              <AnyReactComponent
                key={i}
                lat={marker.lat}
                lng={marker.lng}
                img_src={marker.img_src}
              />
            )
          })}
        </GoogleMapReact>
      </div>
    );
  }
}

export default Mapa;
