import React from 'react';
import linea from '../../media/linea_division.png';
import casa from '../../media/casa_desk.png';
import { Link } from 'react-router';


export class PropiedadesPublicadas extends React.Component {

  render() {
    return (
      <section className="propiedades-publicadas border">
        <div>
          <a className="border">Mi Perfil</a>
          <a className="active">Propiedades Publicadas</a>
          <a className="border">Propiedades Guardadas</a>
          <Link to="/publica_inmueble" className="border">Publicar Propiedad</Link>
        </div>
        <section>
          <h6>Todas mis propiedades</h6>
          <img src={linea} alt="" />

        </section>
        <div className="buscadorProp">
          <input type="text" placeholder="Busca por ubicación o palabra clave"/>
        </div>
        <div className="filtrosProp">
          <article>
            <span>Mostrar por tipo</span>
            <select name="type"
              id="type">
              <option>Seleccione</option>
              <option value="rancho">Rancho</option>
              <option value="casa">Casa</option>
              <option value="departamento">Departamento</option>
            </select>
          </article>
          <article>
            <span>Ordenar por</span>
            <select name="orderBy"
              id="orderBy">
              <option>Seleccione</option>
              <option value="menorPrecio">Menor precio</option>
              <option value="mayorPrecio">Menor precio</option>
            </select>
          </article>
          <article>
            <span>Tipo de operación</span>
            <select name="operationType"
              id="operationType">
              <option>Seleccione</option>
              <option value="renta">Renta</option>
              <option value="venta">Venta</option>
              <option value="preventa">Preventa</option>
            </select>
          </article>
        </div>
        <div className="cuadro border">
          <p className="active">Departamento en Col. del Valle</p>
          <aside>
            <article className="border">
              <img src={casa} alt=""/>
              <p className="active">$16,000,000 MXN</p>
              <span>Ver ficha técnica</span>
            </article>
            <div>
              <article><b>Tipo:</b><span>Rancho</span></article>
              <article><b>Operacion:</b><span>Renta</span></article>
              <article><b>Habitaciones:</b><span>2</span></article>
              <article><b>Baños</b><span>1.5</span></article>
              <article><b>M2</b><span>2</span></article><br />
              <article><b>VIGENTE AL:</b><span>28/12/13</span></article>
              <article><b>ESTADO</b><span>APROBADO</span></article>
            </div>
          </aside>
          <div className="buttons">
            <button>DESTACAR</button>
            <button className="green">DESACTIVAR</button>
            <button>RENOVAR VIGENCIA</button>
            <button className="green">ELIMINAR</button>
          </div>
        </div>
        <div className="cuadro border">
          <p className="active">Departamento en Reforma</p>
          <aside>
            <article className="border">
              <img src={casa} alt=""/>
              <p className="active">$16,000,000 MXN</p>
              <span>Ver ficha técnica</span>
            </article>
            <div>
              <article><b>Tipo:</b><span>Rancho</span></article>
              <article><b>Operacion:</b><span>Venta</span></article>
              <article><b>Habitaciones:</b><span>2</span></article>
              <article><b>Baños</b><span>1.5</span></article>
              <article><b>M2</b><span>2</span></article><br />
              <article><b>VIGENTE AL:</b><span>28/12/13</span></article>
              <article><b>ESTADO</b><span>APROBADO</span></article>
            </div>
          </aside>
          <div className="buttons">
            <button>DESTACAR</button>
            <button className="green">DESACTIVAR</button>
            <button>RENOVAR VIGENCIA</button>
            <button className="green">ELIMINAR</button>
          </div>
        </div>
      </section>
    );
  }
}

export default PropiedadesPublicadas;
