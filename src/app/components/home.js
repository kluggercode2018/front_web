import React from 'react';
import ReactDOM from 'react-dom';
import edificio from '../../media/klugger_gif.gif';
import sol from '../../media/sol_y_luna_desk.png';
import casa from '../../media/casa_desk.png';
import poste from '../../media/lampara1_desk.png';
import pino from '../../media/pino1_desk.png';
import nube from '../../media/nube1_desk.png';
import icon1 from '../../media/icono1_desk.png';
import icon2 from '../../media/icono2_desk.png';
import icon3 from '../../media/icono3_desk.png';
import avion from '../../media/avion_desk.png';
import camion from '../../media/camion_desk.png';
import camion1 from '../../media/camion_phone.png';
import edificio2 from '../../media/edificio2_desk.png';
import edificio2phone from '../../media/edificio2_phone.png';
import nube2 from '../../media/nube2_desk.png';
import nube3 from '../../media/nube3_desk.png';
import SliderSimple from './slider';
import $ from 'jquery';
import { browserHistory } from 'react-router'
import api from'../utils/api';

import update from 'immutability-helper';

//dependences
import Autocomplete from 'react-google-autocomplete';

export class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      responseApi: "",
      form_search:{
        operationType:"",
        propertyType:"",
        latMin: "",
        latMax: "",
        lngMin: "",
        lngMax: "",
        valido: false,
        props:{}
      }
    };
  }

  handleChange = ( data, event ) => {
    this.setState({
      form_search: update(this.state.form_search, {
        [data]: {$set: event.target.value}
      }),
    });
  }

  componentDidMount(){
    ReactDOM.findDOMNode(document.body).scrollIntoView();
  }
  bounds(bounds){
    this.setState({
      form_search: update(this.state.form_search, {
        latMin: {$set: bounds.f.b},
        latMax: {$set: bounds.f.f},
        lngMax: {$set: bounds.b.f},
        lngMin: {$set: bounds.b.b}
      })
    })
  }
  formSearch () {
    this.setState({
      form_search: update(this.state.form_search, {
        latMax: {$set: this.state.form_search.latMax !== "" ? this.state.form_search.latMax : 19.523336305461072},
        latMin: {$set: this.state.form_search.latMin !== "" ? this.state.form_search.latMin : 19.26426286923042},
        lngMax: {$set: this.state.form_search.lngMax !== "" ? this.state.form_search.lngMax : -98.9812006849383},
        lngMin: {$set: this.state.form_search.lngMin !== "" ? this.state.form_search.lngMin : -99.28675793591486},
      }),
    }, () => {
      var obj = this.state.form_search;

      if(obj.operationType === "")
        obj.operationType = "Renta"
      if(obj.propertyType === "")
        obj.propertyType = "Departamento"

      $('.loading').addClass("loadingActive");
      api.rect(obj)

        .then((response)=>{
          $('.loading').addClass("loadingDesactive");
          this.setState({
            responseApi: response
          });
          if(response !== []){
            this.setState({
              form_search: update(this.state.form_search, {
                valido: {$set: true}
              })
            })
          }
        })
    });
  }


  render() {

    if(this.state.form_search.valido === true){
      browserHistory.push({
        pathname: '/catalogo',
        state: {
          props: this.state.responseApi,
          operationType: this.state.form_search.operationType,
          propertyType: this.state.form_search.propertyType,
          params:{
                  latMax : this.state.form_search.latMax,
                  latMin : this.state.form_search.latMin,
                  lngMax : this.state.form_search.lngMax,
                  lngMin : this.state.form_search.lngMin}
        }
      })
    }
    return (
      <section>
        <section>
          <section className="home">
            <div className="div-home">
              <img src={edificio} alt="edificio"/>
              <section>
                <p><b>Klugger</b></p>
                <p>Es una nueva plataforma web sobre todo lo</p>
                <p>inmobiliario en México</p>
                <p>Si estás buscando comprar, vender o rentar tu casa o local, o incluso si eres un desarrollador de proyectos, <b>Klugger ofrecerá las mejores herramientas para que tomes la mejor decisión</b></p>
              </section>
            </div>
          </section>
          <section className="buscador">
            <h5>Encuentra <b>tu propiedad</b></h5>
            <div className="menu">
              <select className="lleno" name="type"
                id="operationType" required
                value={this.state.form_search.operationType}
                onChange={(e) => this.handleChange('operationType', e)}
                >
                <option>Operación</option>
                <option value="Venta">Venta</option>
                <option value="Renta">Renta</option>
              </select>
              <select className="borde" name="type"
                id="tipoPropiedad" required
                value={this.state.form_search.propertyType}
                onChange={(e) => this.handleChange('propertyType', e)}
                >
                <option>Tipo de Propiedad</option>
                <option value="Casa">Casa</option>
                <option value="Casa en condominio">Casa en condominio</option>
                <option value="Departamento">Departamento</option>
              </select>
              <Autocomplete
                className="autocomplete borde"
                onChange={(e) => this.handleChange('location', e)}
                onPlaceSelected={(place) => {
                  var bounds = place.geometry.viewport ? place.geometry.viewport :[];
                  if(bounds !== ""){
                    this.bounds(bounds)
                  }
                }}
                types={['(regions)']}
              />
              <a onClick={this.formSearch.bind(this)}>Buscar</a>

            </div>
          </section>
          <section className="home--two">
            <div className="div-two">
              <img src={sol} alt="sol"/>
              <img src={casa} alt="casa"/>
              <img src={poste} alt="poste"/>
              <img src={pino} alt="pino"/>
              <img src={nube} alt="nube2e"/>
              <img src={icon1} alt="mano"/>
              <img src={icon2} alt="estrella"/>
              <img src={icon3} alt="casa"/>
              <div className="text-slide-cero">
                <p>Encuentra tu <b>camino a casa</b></p>
                <p>Ofreciendo un servicio de localización estratégica</p>
                <p>mediante nuestras bases de datos</p>
              </div>
              <div className="text-slide">
                <p><b>Klugger Web</b></p>
                <p><b>¿Vas a comprar o rentar un inmueble?</b>Compara miles de propiedades y obtén información sobre seguridad, contaminación y todo lo que necesitas saber sobre la zona y el inmueble. Vive ahi antes de mudarte.</p>
              </div>
              <div className="text-slide-two">
                <p><b>Klugger Premium</b></p>
                <p>Toma la mejor  decisión en operaciones inmobilarias.<b>Utiliza nuestras herramientas de analisis desde tu dashboard personalizado.</b>Observa tendencias, estadísticas y predicciones actualizadas constantemente.</p>
              </div>
              <div className="text-slide-three">
                <p><b>Klugger Consultoria</b></p>
                <p><b>Estudios de viabilidad y rentabilidad de desarrollos inmobiliarios</b> basados en modelos de ciencia de datos. Análisis y recomendaciones puntuales para el desarrollo de proyectos residenciales y comerciales de acuerdo a la zona.</p>
              </div>
            </div>
          </section>
          <SliderSimple />
        </section>
        <section className="home--three">
          <div>
            <img src={avion} alt="avion"/>
            <img src={camion1} alt="camion"/>
            <img src={edificio2phone} alt="edificio"/>
            <img src={nube2} alt="nube"/>
            <img src={nube3} alt="nube"/>
            <img src={camion} alt="camion"/>
            <img src={edificio2} alt="edificio"/>
            <div>
            <p>Klugger</p>
              <p>tu mejor opción</p>
              <p>Somos un grupo interdisciplinario compuesto por economistas, cientificos de datos, urbanistas, desarrolladores, arquitectos diseñadores y empresarios con conocimientos en programación, diseño, manejo de datos, así como modelos matemáticos, financieros y estadísticos.</p>
            </div>
          </div>
        </section>
      </section>
    );
  }
}

export default Home;
