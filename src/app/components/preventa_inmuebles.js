import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import circulo from '../../media/circulo_desk.png';
import circle1 from '../../media/circulo4_Preventa_Inmuebles_desk.png';
import circle2 from '../../media/circulo5_Preventa_Inmuebles_desk.png';
import circle3 from '../../media/circulo6_Preventa_Inmuebles_desk.png';
import circle4 from '../../media/circulo7_Preventa_Inmuebles_desk.png';
import circle5 from '../../media/circulo8_Preventa_Inmuebles_desk.png';
import circle6 from '../../media/circulo9_Preventa_Inmuebles_desk.png';
import circle7 from '../../media/circulo10_Preventa_Inmuebles_desk.png';
import circle8 from '../../media/circulo11_Preventa_Inmuebles_desk.png';
import circle9 from '../../media/circulo12_Preventa_Inmuebles_desk.png';
import flecha from '../../media/flecha.png';
import SlideUno from './slide2';
import SlideDos from './slide3';
import SlideTres from './slide4';
import SlideTabletUno from './slide_tablet';
import SlideTabletDos from './slide_tablet_2';
import api from'../utils/api';


export class Preventa extends React.Component {

  componentDidMount(){
    ReactDOM.findDOMNode(document.body).scrollIntoView();
  }
  facebook(){
    api.facebook()
       .then((response) => {
         console.log("reps:::",response);
      });
  }

  render() {
    return (
      <section className="preventa">
        <section className="preventa-inmueble">
          <div>
            <p><b>Preventa</b> para asesores inmobiliarios</p>
            <p>¡Sube tu anuncio y obtén meses gratis en avisos destacados!</p>
            <p>Publica, vende, renta con anticipación y obtén información de calidad para tu inmueble: <b>plazas, parques, restaurantes, servicios, contaminación, etc.</b></p>
            <p>¡Sube tu anuncio y obtén meses gratis</p>
            <p>en avisos destacados!</p>
            <button>CONOCE MÁS</button>
          </div>
        </section>
        <section className="preventa-inmueble-two">
          <p><b>Paquetes</b> para asesores inmobiliarios</p>
          <p><b>Paquetes </b>para asesores</p>
          <p>inmobiliarios</p>
          <SlideDos />
          <div className="circle-blue">
            <div>
              <img src={circulo} alt="5"/>
              <h4>5</h4>
              <p>Recomendaciones</p>
              <b>550 / mes</b>
            </div>
            <div>
              <img src={circulo} alt="10"/>
              <h4>10</h4>
              <p>Recomendaciones</p>
              <b>820 / mes</b>
            </div>
            <div>
              <img src={circulo} alt="15"/>
              <h4>15</h4>
              <p>Recomendaciones</p>
              <b>1200 / mes</b>
            </div>
          </div>
          <div>
            <p><b>Recomendaciones </b>pagadas</p>
            <p><b>Recomendaciones</b></p>
            <p>pagadas</p>
            <SlideUno />
            <SlideTabletDos />
          </div>
          <div className="circle-dos">
            <div>
              <img src={circle1} alt="busqueda"/>
              <p>Sé el primero de tu categoría de búsqueda</p>
            </div>
            <div>
              <img src={circle2} alt="descuentos"/>
              <p>Obtén descuentos especiales por subir más publicaciones</p>
            </div>
            <div>
              <img src={circle3} alt="estadisticas"/>
              <p>Obtén estadísticas de la zona donde se encuentra tu inmueble</p>
            </div>
          </div>
          <div className="circle-dos-down">
            <div>
              <img src={circle4} alt="sugerencia"/>
              <p>Recibe una sugerencia mensual del precio que deberías colocar para que tu inmueble se venda en tiempo y forma, de acuerdo con las tendencias del mercado</p>
            </div>
            <div>
              <img src={circle5} alt="llamadas"/>
              <p>Recibe llamadas de personas interesadas, aún cuando tu publicación esté fuera del mercado</p>
            </div>
          </div>
        </section>
        <section className="preventa-inmueble-three">
          <p><b>Oferta</b> especial</p>
          <p>El lanzamiento oficial de la plataforma es el <b>22 de febrero,</b> sin embargo <b>Por cada 5 subidas</b> en nuestra plataforma, obtén <b>10 días de recomendaciones pagadas gratis.</b> información y análisis especial para tu publicación. Incrementa tus días al incrementar tus subidas:</p>
          <div>
            <p>15 subidas<img src={flecha} alt="flecha"/><b> 1 mes gratis</b> para 5 de tus subidas</p>
          </div>
          <div className="up1">
            <p>30 subidas<img src={flecha} alt="flecha"/><b> 1 mes gratis</b> para 10 de tus subidas</p>
          </div>
          <div className="up2">
            <p>45 subidas<img src={flecha} alt="flecha"/><b> 1 mes gratis</b> para 15 de tus subidas</p>
          </div>
          <div className="up3">
            <p>100 subidas<img src={flecha} alt="flecha"/><b> 1 mes gratis</b> para 30 de tus subidas</p>
          </div>
          <article>
            <span>Para mayores meses o publicaciones, contáctenos a <b>contacto@klugger.mx.</b></span>
            <b>Oferta válida hasta el 28 de marzo.</b>
          </article>
        </section>
        <section className="preventa-inmueble-four">
          <h4>¿Porque publicar con <b>nosotros?</b></h4>
          <div className="middle">
            <article>
              <img src={circle6} alt="inmobiliarias"/>
              <p>Tenemos todas las publicaciones web inmobiliarias de la República Mexicana (alrededor de 2.2 millones de publicaciones)</p>
            </article>
            <article className="circle-6">
              <img src={circle7} alt="informacion"/>
              <p>Ofrecemos el mayor valor agregado en infomación para la toma de desiciones inmobiliarias.</p>
            </article>
            <article className="circle-7">
              <img src={circle8} alt="asesoramiento"/>
              <p>Te asesoramos mediante información del mercado y el precio si tienes una urgencia por vender o rentar tu bien inmueble. Véndelo lo más rápido posible.</p>
            </article>
            <article className="circle-8">
              <img src={circle9} alt="sugerencia"/>
              <p>Obtén el valor de tu inmueble y recibe una sugerencia de precio, cada mes, coherente con el comportamiento del mercado.</p>
            </article>
          </div>
          <SlideTres />
          <SlideTabletUno className="slider-tablet"/>
          <div className="buttons">
            <Link to="/politica_privacidad"><button>POLÍTICAS DE PRIVACIDAD</button></Link>
            <Link to="/publica_inmueble"><button>PUBLICA TU PROPIEDAD</button></Link>
            <Link to="/terminos_condiciones"><button>TÉRMINOS Y CONDICIONES</button></Link>
          </div>
        </section>
        <section className="preventa-inmueble-five">
          <p>Please<b> Sign in</b></p>
          <ul>
            <li><button></button></li>
            <li onClick={this.facebook}><button></button></li>
          </ul>
        </section>
      </section>
    );
  }
}

export default Preventa;
