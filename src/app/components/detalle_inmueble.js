import React from 'react';
import Slider from 'react-slick';
import { Link } from 'react-router';
import { browserHistory } from 'react-router';
import update from 'immutability-helper';
import $ from 'jquery';
import constuction from '../../media/construccion.png';
//import code from '../../media/sketch.png';
import bathroom from '../../media/toilet.png';
import bedroom from '../../media/bed.png';
import parkingLots from '../../media/garage.png';
import halfBathrooms from '../../media/bathtub.png';
import floors from '../../media/stairs.png';
import gardenSize from '../../media/fruit-tree.png';
//import reloj from '../../media/reloj.png';
import camara from '../../media/camarita.png';
import flecha from '../../media/flechita.png';
import laundry from '../../media/washing-machine.png';
import gym from '../../media/barbell.png';
import air from '../../media/air-conditioner.png';

import elevator from '../../media/lift.png';
import heat from '../../media/air-conditioner.png';
import swimmingPool from '../../media/swimming-pool.png';
import security from '../../media/security.png';
import roofGarden from '../../media/roofGarden.png';
import api from'../utils/api';
import {TimelineLite,Sine} from 'gsap';


import GoogleMapReact from 'google-map-react';
import img_src from '../../media/pin.png';
const AnyReactComponent = ({ img_src }) => <div><img src={img_src} className="markers" alt="punto"/></div>;

export class DetalleInmueble extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      hasErrors: false,
      responseApiDetail:"",
      images:[],
      form_contact : {
        name:"",
        email:"",
        ownerEmail:"",
        phone:"",
        url:""
      }
    }
  }
  detalleInmueble() {
    $('.loading').addClass("loadingActive");

    var propertyId = this.props.location.query.propertyId;
    var hashKey = parseInt(this.props.location.query.hashKey,10);
    api.info( propertyId, hashKey )
      .then((response) => {
        if(response !== undefined){
          $('.loading').addClass("loadingDesactive");
          this.setState({
            responseApiDetail: response,
            images: response.imagesUrls
          })
        }else{
          $('.loading').addClass("loadingDesactive");
          browserHistory.push({
            pathname: "/not_found"
          })
        }

      })
  }

  componentDidMount(){
    this.detalleInmueble();
  }
  handleChange = ( data, event ) => {
    if( this.state.form_contact.email !== ""){
      this.emailValidate();
    }else{
      $("#emailValid").fadeOut()
    }
    var state = this.state;
    var propertyId = this.state.responseApiDetail.propertyId,
    hashKey = this.state.responseApiDetail.hashKey;
    var urlInmueble = "http://klugger.mx/detalle_inmueble/?propertyId=" + propertyId + "&hashKey=" + hashKey;
    state.form_contact[ data ] = event.target.value;
    this.setState({
       form_contact : update(this.state.form_contact,{
        ownerEmail: {$set: "nayeli@klugger.mx"},
        //ownerEmail: {$set: this.state.responseApiDetail.mail},
         url: {$set: urlInmueble}
       })
    })
  }

  contact = ( data ) => {
    var info = this.state;
    var isStateEmpty = !info.form_contact.name ||
      !info.form_contact.email ||
      !info.form_contact.ownerEmail ||
      !info.form_contact.phone ||
      !info.form_contact.url;
      // if(!isStateEmpty === false){
      //   alert("tienes que llenar todos los campos")
      // }
    if(!isStateEmpty){
      var obj = this.state.form_contact;
      if(obj.name !== "" && obj.email !== "" && obj.phone !== ""){
        api.contact( obj )
        .then((response) => {
          var tlcontact = new TimelineLite();
          var contactMsg = $(".static--contact-msg");

            tlcontact
            .to(contactMsg, 0.3, {y:"0%", ease:Sine.Elastic})
            .to(contactMsg, 0.3, {y:"100%", delay:4})
        })
      }
    }else{
      this.setState({
        hasErrors: true
      });
    }

  }
  isNumberKey(evt){
    console.log("");
      $(document).ready(function (){
        $('.solo-numero').keyup(function (){
          this.value = (this.value + '').replace(/[^0-9.]/g, '');
        });
      });
  }

  emailValidate() {
    var m = document.getElementById("email").value;
    var expreg = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if(expreg.test(m)){
      $("#emailValid").fadeOut()
    }else{
      if(this.state.form_contact.email !== " "){
        $("#emailValid").fadeIn();
      }
    }
  }

    render() {
      if(this.state.hasErrors){
        var error = 'Este campo es obligatorio';
        if(!this.state.form_contact.name) var errorName = error;
        if(!this.state.form_contact.email) var errorEmail = error;
        if(!this.state.form_contact.phone) var errorPhone = error;
      }

      var props = this.state.images;
      var info = this.state.responseApiDetail;

      const settings = {
        dots: true,
        lazyLoad: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        className:'slider-2, slider-detalle',
        customPaging: function(i) {
          var props = this.slideCount;
          return (
              <a>
                <img src={camara} alt='' className="dot-image"/>
                <p className="dot ">{`${i + 1} de ${props}`}</p>
              </a>
          );
        },
      };

      var constructionInfo = <span>&nbsp;{info.constructionSize}{info.constructionSizeUnit} Construcción total </span>

      if( info.bathrooms ){
        var bathroomInfo = <span>&nbsp;{info.bathrooms} Baño(s)</span>
      }
      if( info.bedrooms ){
        var bedroomInfo = <span>&nbsp;{info.bedrooms} Recámaras</span>
      }
      if( info.parkingLots ){
        var parkingInfo = <span>&nbsp;{info.parkingLots} Estacionamiento(s)</span>
      }
      if( info.halfBathrooms ){
        var halfBathroomsInfo =
        <article>
          <img src={bathroom} alt='' />
          <span>&nbsp;{info.halfBathrooms} medio baño</span>
        </article>;
      }
      if( info.floors ){
        var floorsInfo =
        <article>
          <img src={floors} alt='' />
          <span>&nbsp;{info.floors} pisos</span>
        </article>;
      }
      if( info.gardenSize ){
        var gardenSizeInfo =
        <article>
          <img src={gardenSize} alt='' />
          <span>&nbsp;{info.gardenSize}{info.gardenSizeUnit} jardín</span>
        </article>;
      }
      if( info.description ){
        var description =
        <article>
          <p className="mapa-borde pBack">Descripción</p>
          <div className="datos description">
            <p>{info.description}</p>
          </div>
        </article>;
      }
      if( info.laundry === true){
        var laundryInfo =
        <article>
          <img src={laundry} alt='' />
          <span>&nbsp;Lavandería</span>
        </article>;
      }
      if( info.gym === true){
        var gymInfo =
        <article>
          <img src={gym} alt='' />
          <span>&nbsp;Gimnasio</span>
        </article>;
      }
      if( info.air === true){
        var airInfo =
        <article>
          <img src={air} alt='' />
          <span>&nbsp;Aire acondicionado</span>
        </article>;
      }
      if( info.elevator === true){
        var elevatorInfo =
        <article>
          <img src={elevator} alt='' />
          <span>&nbsp;Elevador</span>
        </article>;
      }
      if( info.heat === true){
        var heatInfo =
        <article>
          <img src={heat} alt='' />
          <span>&nbsp;Calefacción</span>
        </article>;
      }
      if( info.swimmingPool === true){
        var swimmingPoolInfo =
        <article>
          <img src={swimmingPool} alt='' />
          <span>&nbsp;Alberca</span>
        </article>;
      }
      if( info.security === true){
        var securityInfo =
        <article>
          <img src={security} alt='' />
          <span>&nbsp;Seguridad</span>
        </article>;
      }
      if( info.roofgarden === true){
        var roofgardenInfo =
        <article>
          <img src={roofGarden} alt='' />
          <span>&nbsp;Roof Garden</span>
        </article>;
      }
      if(info.roofgarden || info.laundry|| info.gym || info.air || info.elevator || info.heat || info.swimmingPool || info.security === true){
        var servicios =
        <div className="datos services-datos" id="servicios">
          <b>Servicios</b>
          <div>
            {laundryInfo}
            {gymInfo}
            {airInfo}
            {elevatorInfo}
            {heatInfo}
            {swimmingPoolInfo}
            {securityInfo}
            {roofgardenInfo}
          </div>
        </div>;
      }
      if(info.pageSource === "Klugger"){
        var formulario =
        <div className="form">
          <p className="mapa-borde pBack">Contacta al anunciante</p>
          <span style={{"display":"block !important"}}>Hola, vi este inmueble y quiero tener mas información.</span>
          <span style={{"display":"block"}}>Gracias.</span>
          <input type="text" name="name" id="name" placeholder="Nombre" error={errorName}
            defaultValue={this.state.form_contact.name}
            onChange={(e) => this.handleChange('name', e)}/>
          <span className="errorOn">{errorName}</span>

          <input type="email" placeholder="Email" id="email" error={errorEmail}
          defaultValue={this.state.form_contact.email}
          onChange={(e) => this.handleChange('email', e)}/>
          <span className="errorOn">{errorEmail}</span>
          <div id="emailValid">Correo no válido</div>

          <input type="text" placeholder="Teléfono" error={errorPhone}
          className="solo-numero"
          defaultValue={this.state.form_contact.phone}
          onKeyPress={ this.isNumberKey }
          onChange={(e) => this.handleChange('phone', e)}/>
          <span className="errorOn">{errorPhone}</span>

          <a onClick={this.contact.bind(this)} className="contact-a">CONTACTAR AL ANUNCIANTE</a>
          <span style={{"display":"block !important"}}>Al enviar este formulario, aceptas los <Link to="/terminos_condiciones">Términos y condiciones</Link></span>
        </div>;
      }else{
        //aqui va la url
        formulario = <div></div>;
      }

      var center = {
        lat: info.lat,
        lng: info.lng
      };
      var zoom = 15;
      if(center.lat || center.lng !== undefined){
        var googleMapElement =
        <GoogleMapReact
          bootstrapURLKeys={{ key:'AIzaSyBjPqgfje7_Dg_qlZ9eiie7zLpw-9JN0SA' }}
          defaultCenter={center}
          defaultZoom={zoom}
        >
        <AnyReactComponent
          lat={center.lat}
          lng={center.lng}
          img_src={img_src}
        />
        </GoogleMapReact>;
      }


      var images = props.map( (data, i) => (
        <div key={i}>
            <img src={data} alt="" />
        </div>
      ));
      if(this.state.images.length >= 1){
        var slide = <Slider {...settings} >
          {images}
        </Slider>;
      }
        return (
          <section>
            <section className="detail">

              <div className="detail--first-part">
                <div className="images borde slider-detalle">
                  {slide}
                  <p className="price-detalle">${info.price}&nbsp;<span className="price-detalle-unit">{info.priceUnit}</span></p>
                </div>
                <div className="location">
                  <div className="border">
                    <span>Klugger <img src={flecha} alt='' /> {info.propertyType} <img src={flecha} alt='' /> {info.propertyState} <img src={flecha} alt='' /> {info.municipality}</span>
                  </div>
                </div>

                <div className="services">
                <p className="services-borde pBack  datosP">Datos principales</p>
                  <div className="datos services-datos" style={{ 'backgroundColor':'#e8e8e6'}}>
                    <article>
                      <img src={constuction} alt='' />
                      {constructionInfo}
                    </article>
                    <article>
                      <img src={halfBathrooms} alt='' />
                      {bathroomInfo}
                    </article>
                    <article>
                      <img src={bedroom} alt='' />
                      {bedroomInfo}
                    </article>
                    <article>
                      <img src={parkingLots} alt='' />
                      {parkingInfo}
                    </article>
                    {halfBathroomsInfo}
                    {floorsInfo}
                    {gardenSizeInfo}
                  </div>
                  {servicios}
                </div>
                {/*<div className="datos datos-anunciante" style={{ "backgroundColor":"#e8e8e6"}}>
                  <strong>Datos del anunciante</strong>
                  <p>GRUPO URBANA</p>
                  <article>
                    <img src={code} alt='' />
                    <span>Código del anunciante</span>
                    <p>49392969</p>
                    <span>Código Klugger</span>
                    <p>28462739</p>
                    <img src={reloj} alt='' />
                    <span>Publicado hace 6 días</span>
                  </article>
                </div>*/}
              </div>
              <div className="detail--second-part">
                <div className="mapa">
                  <p className="mapa-borde pBack ubicacion" style={{"top":"2em", "position":"relative"}}>Ubicación</p>
                  <div className="mapa-div">
                    {googleMapElement}
                  </div>
                </div>
                {description}
                {formulario}
              </div>
            </section>
            <section className="static--contact-msg">
              <header>
                <h4>¡Gracias!</h4>
                <p>Su mensaje ha sido recibido por el vendedor.</p>
              </header>
            </section>
          </section>
        )
    }
}

export default DetalleInmueble;
