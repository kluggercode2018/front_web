import React, { Component } from 'react';
import Slider from 'react-slick';
import circulo from '../../media/circulo_desk.png';


class SliderSimple extends Component {

  render() {
    const settings = {
      dots: true,
      lazyLoad: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      className:'slider-3'
    };
    return (
        <Slider {...settings}>
          <div>
            <img src={circulo} alt="5"/>
            <h4>5</h4>
            <p>Recomendaciones</p>
            <b>550 / mes</b>
          </div>
          <div>
            <img src={circulo} alt="10"/>
            <h4 className="h4">10</h4>
            <p>Recomendaciones</p>
            <b>820 / mes</b>
          </div>
          <div>
            <img src={circulo} alt="15"/>
            <h4 className="h4">15</h4>
            <p>Recomendaciones</p>
            <b>1200 / mes</b>
          </div>
        </Slider>
    );
  }
}

export default SliderSimple;
