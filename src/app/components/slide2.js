import React, { Component } from 'react';
import Slider from 'react-slick';
import circle1 from '../../media/circulo4_Preventa_Inmuebles_desk.png';
import circle2 from '../../media/circulo5_Preventa_Inmuebles_desk.png';
import circle3 from '../../media/circulo6_Preventa_Inmuebles_desk.png';
import circle4 from '../../media/circulo7_Preventa_Inmuebles_desk.png';
import circle5 from '../../media/circulo8_Preventa_Inmuebles_desk.png';

class SliderSimple extends Component {

  render() {
    const settings = {
      dots: false,
      lazyLoad: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      className:'slider-2'
    };
    return (
        <Slider {...settings}>
          <div><img src={circle1} alt="busquedas"/><p>Sé el primero de tu categoría de búsqueda</p></div>
          <div><img src={circle2} alt="descuentos"/><p>Obtén descuentos especiales por subir más publicaciones</p></div>
          <div><img src={circle3} alt="estadisticas"/><p>Obtén estadísticas de la zona donde se encuentra tu inmueble</p></div>
          <div><img src={circle4} alt="sugerencia"/><p>Recibe una sugerencia mensual del precio que deberías colocar para que tu inmueble se venda en tiempo y forma, de acuerdo con las tendencias del mercado</p></div>
          <div><img src={circle5} alt="llamadas"/><p>Recibe llamdas de personas interesadas, aún cuando tu publicación esté fuera del mercado</p></div>
        </Slider>
    );
  }
}

export default SliderSimple;
