import React, { Component } from 'react';
import Slider from 'react-slick';
import circle6 from '../../media/circulo9_Preventa_Inmuebles_desk.png';
import circle7 from '../../media/circulo10_Preventa_Inmuebles_desk.png';
import circle8 from '../../media/circulo11_Preventa_Inmuebles_desk.png';
import circle9 from '../../media/circulo12_Preventa_Inmuebles_desk.png';
// import PrevArrow from '../../media/arrow1_phone.png';
// import NextArrow from '../../media/arrow2_phone.png';

class SliderSimple extends Component {

  render() {
    const settings = {
      dots: true,
      lazyLoad: true,
      infinite: false,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 2,
      className:'slider-tablet'

    };
    return (
        <Slider {...settings}>
          <div><img src={circle6} alt="publicaciones"/><p>Tenemos todas las publicaciones web inmobiliarias de la República Mexicana (alrededor de 2.2 millones de publicaciones)</p></div>
          <div><img src={circle7} alt="Informacion"/><p>Ofrecemos el mayor valor agregado en infomación para la toma de desiciones inmobiliarias.</p></div>
          <div><img src={circle8} alt="asesoramiento"/><p>Te asesoramos mediante información del mercado y el precio si tienes una urgencia por vender o rentar tu bien inmueble. Véndelo lo más rápido posible.</p></div>
          <div><img src={circle9} alt="sugerencia"/><p>Obtén el valor de tu inmueble y recibe una sugerencia de precio, cada mes, coherente con el comportamiento del mercado.</p></div>
        </Slider>
    );
  }
}

export default SliderSimple;
