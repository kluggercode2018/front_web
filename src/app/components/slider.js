import React, { Component } from 'react';
import Slider from 'react-slick';
import icon1 from '../../media/icono1_desk.png';
import icon2 from '../../media/icono2_desk.png';
import icon3 from '../../media/icono3_desk.png';

class SliderSimple extends Component {

  render() {
    const settings = {
      dots: false,
      lazyLoad: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      className:'slider-2'
    };
    return (
      <Slider {...settings} >
        <div>
          <img src={icon1} alt="icono mano"/>
            <p><b>Klugger Web</b></p>
            <p><b>¿Vas a comprar o rentar un inmueble?</b></p>
            <p>Compara miles de propiedades y obtén información sobre seguridad, contaminación y todo lo que necesitas saber sobre la zona y el inmueble. Vive ahi antes de mudarte.</p>
        </div>

        <div>
          <img src={icon2} alt="icono estrella"/>
            <p><b>Klugger Premium</b></p>
            <p>Toma la mejor  decisión en operaciones inmobilarias.<b>Utiliza nuestras herramientas de analisis desde tu dashboard personalizado.</b>Observa tendencias, estadísticas y predicciones actualizadas constantemente.</p>
        </div>

        <div>
          <img src={icon3} alt="icono casa" /><p><b>Klugger Consultoria</b></p><p><b>Estudios de viabilidad y rentabilidad de desarrollos inmobiliarios</b> basados en modelos de ciencia de datos. Análisis y recomendaciones puntuales para el desarrollo de proyectos residenciales y comerciales de acuerdo a la zona.</p>
        </div>

      </Slider>
    );
  }
}

export default SliderSimple;
