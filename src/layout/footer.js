import React from 'react';
import { Link } from 'react-router-dom';
import footer from '../media/linea_desk.png';
import footer2 from '../media/redes_footer_desk.png';


export class Footer extends React.Component {
  render() {
    return (
      <footer>
          <img src={footer} alt="linea"/>
          <Link to="https://www.facebook.com/Klugger-183785829089688/" target="_blank"><img src={footer2} alt="redes sociales"/></Link>
          <p>&#169; 2018 Klugger. All rights reserved</p>
      </footer>
    );
  }
}

export default Footer;
