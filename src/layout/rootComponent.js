import React from "react";
import HeaderComponent from './headerComponent';
import Loading from '../app/static/loading';
import Footer from './footer';

export class RootComponent extends React.Component {
  render() {
    return (
        <main>
          <Loading />
          <HeaderComponent />
            {this.props.children}
          <Footer />
        </main>
    );
  }
}

export default RootComponent;
