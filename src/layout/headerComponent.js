import React from 'react';
import { Link } from 'react-router';
import logo from '../media/desk_logo.png';
import $ from 'jquery';


export class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      condition: false
    };
  }
  handleClick() {
    this.setState({
      condition: !this.state.condition
    });

    var enlaces = $('#enlaces'),
    icono = $('#btn-menu .icono');
    if (this.state.condition === false) {
      enlaces.show();
      icono.addClass('fa-times');
      icono.removeClass('fa-bars');
    }else{
      enlaces.hide();
      icono.addClass('fa-bars');
      icono.removeClass('fa-times');
    }
  }

  render() {
    return (
      <header id="header">
        <nav className="menu">
          <div className="logo">
            <Link to="/"> <img src={logo} alt="logo" /></Link>
            <Link to="" className="btn-menu" id="btn-menu" onClick={this.handleClick.bind(this)}><i className="icono fas fa-bars" /></Link>
          </div>
          <div className="enlaces" id="enlaces">
            <Link to="/"onClick={this.handleClick.bind(this)}>Inicio</Link>
            <Link to="/publica_inmueble"onClick={this.handleClick.bind(this)}>Publicar Inmueble</Link>
            {/*<Link to="/preventa_inmuebles"onClick={this.handleClick.bind(this)}>Preventa inmueble</Link>*/}
            <Link to="/servicios"onClick={this.handleClick.bind(this)}>Servicios</Link>
            <Link to="http://klugger-4624997.hs-sites.com/blog" target="_blank" onClick={this.handleClick.bind(this)}>Blog</Link>
          </div>
        </nav>
      </header>
    );
  }
}

export default Header;
