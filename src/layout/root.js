import React from "react";
import Header from './header';
import Loading from '../app/static/loading';
import Footer from './footer';

export class Root extends React.Component {
  render() {
    return (
        <main>
          <Loading />
          <Header />
            {this.props.children}
          <Footer />
        </main>
    );
  }
}

export default Root;
