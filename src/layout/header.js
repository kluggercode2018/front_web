import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../media/desk_logo.svg';


export class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      condition: false
    };
  }

  render() {
    return (
      <header id="header">
        <nav className="menu">
          <div className="logo">
            <Link to="/"> <img src={logo} alt="logo" /></Link>
          </div>
        </nav>
      </header>
    );
  }
}

export default Header;
